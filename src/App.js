import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link, Switch, Redirect } from 'react-router-dom';

import Home from './Mappincontainer/Home';

import Authenticity from './component/Authenticity';
import Header from './component/Header';

import Footers from './component/Footers';
import TrackOrder from './component/TrackOrder';
import Careers from './component/Careers';
import RefundPolicy from './component/RefundPolicy';
import PrivacyPolicy from './component/PrivacyPolicy';
import Cart from './component/CartPage';
import BecomeDealer from './component/BecomeDealer';
import Signin from './component/signin';
import SignUp from './component/signup';
import MyAccount from './component/MyAccount';
import CustomersSupport from './component/CustomersSupport';
import Checkout from './component/checkout';
import Deal from './component/Deal';
import Terms from './component/Terms';
import Overview from './component/Overview';
import AllProducts from './component/AllProducts';
import DetailPage from './component/DetailPage';
import Accord from './component/Accord';
import Blogsection from './component/Blogsection';
import MultiFunctionProduct from './component/AllProducts/multifuncitonproduct';
import Products from './component/AllProducts/Products';
import FiltrationProduct from './component/AllProducts/multifuncitonproduct';
import Wishlist from './component/wishlist';
import AfterNav from './component/AfterNav';
import BlogPage from './component/BlogPage';
import ContactForm from './component/ContactForm';
import Copyrights from './component/Copyrights';
import Footer from './component/Footer';
import Mobo_Footer from './component/Mobo_Footer';
import PageDes from './component/PageDes';
import EnquiryForm from './component/EnquiryForm';
import TestDetail from './component/TestDetail';
import MobileFooter from './component/MobileFooter';
import { Navbar } from 'react-bootstrap';
import CompanyPolicy from './component/CompanyPolicy'
import Privacy from './component/Privacy';
import Policies from './component/Policies';
import NotFound from './component/404NotFound';






function App() {
  return (
    <div className="App" style={{background:"white"}}>
          

          <Router>
       
           {/* <BlogPage/> */}
{/* <TestDetail/> */}
             {/* <PageDes/> */}
            {/* <EnquiryForm/> */}
           {/* <DetailPage/> */}
        <Header/>
        <AfterNav/>
        {/* <Navbar/> */}
       
   {/* <TestDetail/> */}

            <Switch>
              <Route exact path='/' component={Home}></Route>
              <Route exact path='/home' component={Home}></Route>
              <Route exact path='/detailpage' component={DetailPage}></Route>
              <Route exact path='/enquiryform' component={EnquiryForm}></Route>
              <Route exact path='/sigin' component={Signin}></Route>
              <Route exact path='/signup' component={SignUp}></Route>
              <Route exact path='/cartvalue' component={Cart}></Route>
              <Route exact path='/customersupport' component={CustomersSupport}></Route>
              <Route exact path='/myaccount' component={MyAccount}></Route>
              <Route exact path='/becomedealer' component={BecomeDealer}></Route>
              <Route exact path='/checkout' component={Checkout}></Route>
              <Route exact path='/authenticity' component={Authenticity}></Route>
              <Route exact path='/carrer' component={Careers}></Route>
              <Route exact path="/refund&policy" component={RefundPolicy}></Route>
              <Route exact path='/privacy&policy' component={Policies}></Route>
              <Route exact path='/deals' component={Deal}></Route>
              <Route exact path='/terms&condition' component={Terms}></Route>

              <Route path="/productpage" component={(props)=><DetailPage {...props} key={window.location.pathname}/>}></Route>

              <Route exact path='/allproducts' component={AllProducts}></Route>
              <Route exact path='/frequentlyaskquestions' component={Accord} ></Route>
              <Route exact path="/blogsection" component={Blogsection} ></Route>
              {/* <Route exact path="/blogpage" component={BlogPage} ></Route> */}
              <Route path='/individualblogpage/:id' component={(props)=><BlogPage {...props} key={window.location.pathname} />}  ></Route>

              <Route exact path='/wishlist' component={Wishlist} ></Route>
              <Route exact path='/trackorder' component={TrackOrder} ></Route>
              <Route exact path='/overview' component={Overview} ></Route>
              <Route  component={NotFound}></Route>
            </Switch>
            
{/* <Mobo_Footer/> */}
<MobileFooter/>
            <Footer/>
           
              <Copyrights/>
          </Router>
    </div>
  );
}

export default App;
