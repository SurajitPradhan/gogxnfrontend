import React, { Component } from 'react'
import './style.css'

export default class HomePageContent extends Component {
    render() {
        return (
          <React.Fragment>
              
              <div className="content_container py-4 " >

                  <div >

                 <div >
                 <h4  style={{fontWeight:'bold'}}>Greenex Nutrition – India’s Most Trusted Health Supplement Brand </h4>
                 </div>
                  <article className="home_article" >One brand you cannot miss is  <span className='marktext'>GXN</span> when it comes to trust a brand for the 
                      highest quality health supplement for your bodybuilding goal. GXN is
                       an Indian brand that deals with health supplements & sports nutrition. 
                       <span className='marktext'>GXN</span> provides the complete range of dietary supplements such as whey protein,
                        mass gainers, fat burners, BCAA, Creatine, Energy drinks, Vitamins & minerals.
                    </article>

                    <h4  style={{fontWeight:'bold'}} >Categories We Offer  </h4>
                    <p className="contents_para"  >Regardless of what nutrition your training demands, 
                        you will find it in the  <span className='marktext'>GXN's</span>  Supplement collection.
                         The innovation & commitment provides you with a variety of items
                          for each category you can select depending on the level and criteria 
                          of your training. You can select from the categories of GXN supplements:
                          <ol className="contents_para pl-2">
                           <li >Mass Gainers </li>
                           <li >Rapid Gain</li>
                           <li >Fat Burners </li>
                           <li >Hyper Ripped</li>
                           </ol>
                      </p>

                    

                   
                      <h4  style={{fontWeight:'bold'}}>Whey Protein </h4>
                      <p className="home_article">A protein supplement is a must for anyone who leads an active lifestyle.
                           A physically active person needs a certain amount of protein to ensure that
                            the body is continuously fixing itself so that it can function well and produce
                             fast results. In the  <span className='marktext'>GXN</span>  Protein range, you'll find high-quality whey protein 
                             supplements like Whey Protein Isolate, Whey Protein Concentrates, and even 
                             Protein Blends. Protein supplement products contain a high concentration
                              of high-quality proteins obtained from reputable sources. 
                              They are quickly consumed by the body and are the most 
                              efficient way to get the extra protein you need.
                             
                              </p>
                              </div>

                       
                   
              </div>

            

          </React.Fragment>
        )
    }
}
