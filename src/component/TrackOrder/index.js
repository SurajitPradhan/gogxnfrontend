import React, { Component } from 'react'
import './style.css'
export class TrackOrder extends Component {
    render() {
        return (
            <div className="trackorder">
                <h1 className="track mt-3">Track Your Order</h1>
                <p className="procedure"> To track your order please enter your Order ID in the box below and press the "Track" button. 
                    This was given to you on your receipt and in the confirmation email you should have received.</p>
                    <div className="script"><center>
                    <div id="pickrr-tracking-container"> 
                    <div id="pickrr-tracking-radio-group"> 
                    <input type="radio" id="tracking_id" name="pickrr-query-type" value="tracking_id" checked/> 
                    <label for="tracking_id">Track ID</label>
                     <input type="radio" id="client_order_id" name="pickrr-query-type" value="client_order_id"/> 
                     <label for="client_order_id">Order ID</label>
                      </div>
                      <input id="pickrr-tracking-input"/> 
                     

                            <button id="pickrr-tracking-btn" 
style={{ height: "40px", 
width: "150px", 
fontsize: "15px", 
padding: "8px 57px",
 borderRadius: "5px",
 color: "#272727", 
border: "1px solid #fdaf23",
 background: "#fdaf23"}} > Track </button>
                            </div>
                    {/* <script src="https://widget.pickrr.com/script.js"></script>
                            */}
                    </center>   </div></div>
        )
    }
}

export default TrackOrder

