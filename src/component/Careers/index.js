import React, { Component } from 'react'
import { Redirect, withRouter } from 'react-router';
import careers from '../../images/careers.jpg'
import './style.css';

export class Careers extends Component {

    constructor(){
        super()
        this.state={
            EnquiryForm:false
        }
        this.CarreerSection=this.CarreerSection.bind(this)
    }

    CarreerSection(){
        this.setState({
            EnquiryForm:true
        })
    }



    render() {
        return (
            <div className="career_details " ><div>

                {this.state.EnquiryForm?<Redirect to='/enquiryform' />:''}
                <img className="img-fluid careers  pb-3" src={careers} alt="rep" /></div>
            
                <p className="para_careers mt-2">An inclusive work culture is the first and promising philosophy of Greenex Nutrition.</p>
                <p className="para_careers ">Beginners are guided by the core professionals in a cumulative way where they could succeed in
                    building the rigid base of a professional career for flourishing growth.</p>
                <p className="para_careers ">Our tiny initiatives to major goals are driven and completed by the ideas and strategies of 
                    our employees. Their recommendations are heard, polished and shaped by the managements very
                    constructively to encourage the ideas of all.</p>
                    <p className="para_careers ">Most conducive atmosphere which would inspire and motivate you for developing your
                        potential to the fullest, which you would definitely like to do.</p>
                        <p className="para_careers ">We are committed to building lifelong relationships with our professionals whose 
                            contributions towards building the GXN are commendable and praiseworthy.</p>
                            <p className="para_careers ">Salary and perks are totally based on your results/achievements. Unlimited
                                opportunities for your professional & personal growth.</p>
                                <p className="para_careers ">Let’s Join the dynamic team of experts and undisputed leader for the thriving experience to give your career a justifiable recognition</p>
                                
                                <p className="opportunitites d-none py-4"><b>CURRENT OPPORTUNITIES:-</b></p>
                            

                            
<div class="bs-example d-none">
    <table class="table table-hover">
        <thead>
            <tr className="tables  ">
                        <th className="head px-2">S.No</th>
                        <th className="head px-2">JOB ID</th>
                        <th className="head px-2">JOB TITLE</th>
                        <th className="head px-2">JOB LOCATION</th>
                        <th className="head ">JOB DESCRIPTION</th>
                        <th className="head px-2">JOB CREATION</th>
                        <th className="head px-2">STATUS</th>
                        <th className="head px-2">APPLY HERE</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                            <th className="one py-4 px-4" scope="row">1.</th>
                            <td >	GXN 2510</td>
                            <td>Sr Sales Executive</td>
                            <td>Guwahati</td>
                            <td>	Cold calling and business development in seven states of north east.</td>
                            <td>1 June 2019	</td>
                            <td>CLOSED</td>
                        <td><a href="#">Click Here</a></td>
            </tr>
            <tr>
            <th className="one py-4"  scope="row">2.</th>
                <td>	GXN 2511</td>
                <td>Sr Sales Executive</td>
                <td>Kolkata</td>
                <td>Cold calling and business development in west bengal, Orrisa, Jharkhand, Bihar</td>
                <td>1 June 2019</td>
                <td>CLOSED</td>
                <td><a href="#">Click Here</a></td>
            </tr>
            <tr>
            <th className="one py-4"  scope="row">3.</th>
                <td>	GXN 2512</td>
                <td>Sr Sales Executive</td>
                <td>Delhi</td>
                <td>Cold calling and business development in All states.</td>
                <td>1 June 2019</td>
                <td>CLOSED</td>
                <td><a href="#">Click Here</a></td>
            </tr>    
            <tr>
            <th className="one py-4"  scope="row">4.</th>
                <td>GXN 2513	</td>
                <td>Sr Sales Executive</td>
                <td>Sonepat</td>
                <td>Cold calling and business development in Sonepat and nearby cities.</td>
                <td>1 June 2019</td>
                <td>CLOSED</td>
                <td><a href="#">Click Here</a></td>
                                            </tr>
        
        </tbody>
    </table>
</div>
<hr/>
                                <div className="what_we_offer ">
                            <p> <b> What we offer to our employee:</b></p>  
                            <ol className="our_offers py-2 px-5">
                                <li>Good incentives and stipend</li>
                                <li>Competitive salary.</li>
                                <li>Health insurance to cover you and your family.</li>
                                <li>Flexible leave policy.</li>
                                <li>Strategical increment over a stipulated period.</li>
                                <li>Experts training to bring out your best.</li>

                            </ol>
                            <p className="text mb-3">If you love the exciting work culture and ready to face the manifold of 
                                challenging opportunity, then Greenex Nutrition is just few step closer to you.</p>
                                <button onClick={this.CarreerSection} type="button" class="btn btn-info mb-5">Apply Now</button>


</div>  
                                </div>
        )
    }
}

export default withRouter(Careers)

