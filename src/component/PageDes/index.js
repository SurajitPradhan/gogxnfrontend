import axios from 'axios'
import React, { Component } from 'react'
import './style.css'
import server from '../server/server';
export class PageDes extends Component {

  constructor(){
    super()
    this.state={
      allProducts:[]
    }
  }


  componentDidMount(){
    axios.get(`${server}api/product/`).then((res)=>{
      let product=[]
      for(var i=0;i<res.data.length;i++){
        if(this.props.data==res.data[i].name){
          product.push({name:i,value:res.data[i]})
        }
      }
    
      this.setState({
        allProducts:product
      })

    })
  }

   
    render() {
     
       
        return (

          <React.Fragment>
        {
          this.state.allProducts.map((item)=>{

            return(
            <div className="product_overviews ">
          HEloo
    
            <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck1"/>
                <label class="product_tab-label" for="chck1">Product Overview</label>
                <div class="product_tab-content overflow-auto" >
                    <p className="product_contents">
                      <div dangerouslySetInnerHTML={{__html:item.value.description}} />
                </p>
                </div>
              </div>
           
              </div>
        
          
        
        
              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck2"/>
                <label class="product_tab-label" for="chck2">Ingredients</label>
                <div class="product_tab-content overflow-auto">
                    <p className="product_contents">
                Yes, we are a prominent supplier and manufacturer of all kinds of Health’s nutrition including mass gainer, whey protein, fat burner and workout essentials.
                </p>
                </div>
              </div>
           
              </div>
        
           
              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck3"/>
                <label class="product_tab-label" for="chck3">Nutritional Information</label>
                <div class="product_tab-content overflow-auto" style={{textAlign:'center'}}>
                   
                 <img src={item.value.Nutritional_information} height='auto' width='500px' alt='image'/>
              
                </div>
              </div>
           
              </div>
        
              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck4"/>
                <label class="product_tab-label" for="chck4">Why Choose?</label>
                <div class="product_tab-content overflow-auto">
                <p className="contents">

                <ul class="list-group">
           <li class="list-group-item" >Free Fast Delivery</li>
        <li class="list-group-item">Good Quality Product</li>
           <li class="list-group-item">Find the Original Product </li>
          <li class="list-group-item">Hashle Free Return</li>
           <li class="list-group-item">This is a Complete Natural Product</li>
            </ul>
                 </p>
                </div>
              </div>
           </div>
           
        
            </div>)
          })
        }

    </React.Fragment>

        )
    }
}

export default PageDes
