import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './style.css'
export class MobileFooter extends Component {
    render() {
        return (
            <div className="mobile_footer px-5">
                <div className="mobile-view row">
                    <div className="col-12 ">
                       <div className="row">
                           <div className="col-6">
                     
                       {/* <div className= " footer_row row" style={{fontSize:"10px"}}>
                           */}
                           <p><Link to="/trackorder" ><strong style={{color:"red",  fontSize:"11px"}}>Track your order</strong></Link></p>       
                           <p><Link to="/carrer" ><strong style={{color:"white", fontSize:"11px"}}>Careers</strong></Link></p>  
                           <p><Link to="/frequentlyaskquestions" ><strong style={{color:"white",  fontSize:"11px"}}>FAQ</strong></Link></p>   
                          
                            {/* </div> */}
                           </div>
                           <div className="col-6">
                         
                           <p><Link to="/becomedealer" ><strong style={{color:"red", fontSize:"11px"}}>Distributionship</strong></Link></p>       
                           <p><Link to="/terms&condition" ><strong style={{color:"white",  fontSize:"11px"}}>Terms & Conditions</strong></Link></p>  
                           <p><Link to="/privacy&policy" ><strong style={{color:"white",  fontSize:"11px"}}>Privacy & Policy</strong></Link></p>  
                           <p><Link to="/refund&policy" ><strong style={{color:"white",  fontSize:"10px"}}>Refund & Cancellation</strong></Link></p>  
                               </div>
                               <div class="horizontal-rule" ></div>
                              <div className="row">
                              
                               <div className="social_links ">
                                   {/* <p className="get_sociall" style={{fontSize:"12px", color:"white"}}><b> Get Social</b></p> */}
                                   <div className="socials_links px-4    mt-2">
                                   <center>
                            <a href="" >  <i class="fa fa-facebook-square  fa-2x" aria-hidden="true" style={{textAlign:"center", padding:"2px", color:"white"}}></i></a>
                            <a href="" >   <i class="fa fa-youtube-play fa-2x" aria-hidden="true" style={{textAlign:"center", padding:"2px",  color:"white"}}></i></a>
                            <a href="" >  <i class="fa fa-instagram fa-2x" aria-hidden="true" style={{textAlign:"center", padding:"2px",  color:"white"}}></i></a>
                            <a href="" >    <i class="fa fa-twitter-square fa-2x" aria-hidden="true" style={{textAlign:"center", padding:"2px",  color:"white"}}></i></a>
                               <a href="" >  <i class="fa fa-pinterest-square fa-2x" aria-hidden="true" style={{textAlign:"center", padding:"2px",  color:"white"}}></i></a>
                               </center>
</div>
                             
                               </div>
                               <div className="make_subscribe  mt-2">
                                   {/* <p className="click_to_sub" style={{color:"white", fontSize:"12px"}}><b> Subscribe Us</b></p> */}
                                   <div className="sub_form px-4 mt-2">
                               <form action="">
                            <input type="text" class="form-control " id="inputnamel4" placeholder="Name"/>
                            <input type="email" class="form-control mt-2" id="inputEmail4" placeholder="Email"/>
                            <button type="button" class="btn btn-primary mt-2 mb-2">Subscribe</button>
                            </form> 
                            </div>
                               </div>
                               </div>
                       </div>
                    </div>
                  

                    </div>                
            </div>
        )
    }
}

export default MobileFooter
