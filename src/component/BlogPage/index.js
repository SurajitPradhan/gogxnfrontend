import axios from 'axios'
import React, { Component } from 'react'
import server from '../server/server'
import './style.css'
export class BlogPage extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             data:[],
             name:this.props.match.params.id,
        }
    
        
    }
    componentDidMount(){
        
        axios.get(`${server}api/individualblogpage/`)
        .then((res)=>{
            var blogname=this.state.name
            let datavalue=[]
           for (var i = 0; i < res.data.length; i++){
               
               if(blogname==res.data[i].title){
                datavalue.push({name:i,value:res.data[i]})
               }
           }

           this.setState({
                 data:datavalue
           })
         
        })
    }
   
    render() {
        return (<>        
                    <div className="indv_blog ">
             <div className="blogss row">
                    {
                            this.state.data.map((item)=>{
                                return(<> 
                                 <img src={item.value.image} className="img-fluid" style={{width:'100%'}}/>
                                 <div dangerouslySetInnerHTML={{__html:item.value.text}} />
                                 
                              </>  )
                            })
                        }
                    
              

             
                    </div>
                    </div>
             
               
                 
            </>
        )
    }
}

export default BlogPage
