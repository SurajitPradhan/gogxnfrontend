import React, { Component } from 'react'
import './style.css';
import authbanner from '../../images/authenticity.jpg'
import { Redirect } from 'react-router';

export class Authenticity extends Component {
  


    render() {
        return (
            <React.Fragment>
             
            <div className="authenticity">
                <div className="bannerportion" >
                    <img src={authbanner}  style={{cursor:'pointer'}} className="img-fluid careers" height="300px"/>
                </div>
                <div className="auth_steps">
                    <h1 className="check">CHECK YOUR PRODUCT'S AUTHENTICATION</h1>
                    <p className="steps">GXN believes on the fact that the authenticity of any nutrition 
                    product is the most important concern while purchasing health product online. Our continuous & successful 
                    efforts of providing 100% authentic nutrition made us India’s most trusted supplement brand.
                     GXN is certified by all Government and top certification agencies like FSSAI, HACCP, GMP, 
                     & ISO.</p>
                     <p className="step_one mt-2"> <b>What is Products Authentication?</b></p>
                     <p className="steps">Authentication is a process of validating the product that it is original product made by the 
                         respective brand and not a counterfeit product.</p>
                         <p className="step_one mt-2"><b>How to Authenticate GXN Products?</b></p>
                         <p className="steps"><b>Step 1:</b> Find the QR code affixed at the protection seal, inside cap of the box.</p>
                         <p className="steps"><b>Step 2:</b> Scan the QR code with any QR scanner</p>
                         <p className="steps"><b>Step 3:</b> If the product is originally manufactured from GXN it will show you a message 
                         <b>“Your product is 100% authentic”</b> on your phone’s screen. If the product is counterfeit and not manufactured by GXN, it will not authenticate over scanning .On Scanning the QR code Along with 
                         the authentication you will also get to see the product serial number and manufacturing date of the products.</p>
                         <p className="step_one mt-2"><b>Why Authentication is important?</b></p>
                         <p className="steps">Fake products & bogus claim could be a threat to consumer health. The increasing level of counterfeit activities in all sectors poses a huge challenge to industry and governments around the world. Especially with today‘s online sales & purchase models, where e-commerce 
                         platforms facilitate the initial transaction between buyers and sellers without any physical appearance.</p>
                         <p className="steps">This is where the process of authentication prevents any further mishap if your products turned out to be not genuine. It is not only helpful
                              for the health & wellness concern but also gives mental peace even if the product is authentic.</p>
                              <p className="steps">We at GXN keep product authenticity process as on top priority for making the users confident before 
                                  using any product in the name of GXN.</p>
                                 
                                       <p className="steps mb-5 mt-3"><b>Disclaimer:</b>All GXN products are manufactured at FSSAI approved manufacturing facilities and are not intended to diagnose, 
                                       treat, cure, or prevent any disease. Please read product packaging carefully before purchase and use. The 
                                       information/articles on GXN’s products label or domain <a href="">(www.gogxn.com)</a> is provided for informational purpose
                                        only and is not meant to substitute for the advice provided by your doctor or other healthcare professional. 
                                           These statements are not ratified by any government agency and are for general guidance only.</p>
                </div>
            </div>
            </React.Fragment>
        )
    }
}

export default Authenticity;
