import React,{Component} from 'react';
import '../CartPage/style.css'
import logo4 from '../../images/logo4.png'
import gxn1 from '../../images/gxn4.jpeg'
import { Link, Redirect } from 'react-router-dom';
import Button from '@material-ui/core/Button'
import Header from '../Header';
import axios from 'axios';
import server from '../server/server';
class Cart extends Component{
    constructor(){
        super();
        this.state={
               Product:[],
               cartValue:0, 
               productclick:false, 
               productname:''
        }

        this.Remove=this.Remove.bind(this)
        this.MakeDecrease=this.MakeDecrease.bind(this)
        this.MakeDecrease=this.MakeDecrease.bind(this)
        this.ProductClick=this.ProductClick.bind(this)
        
    }


    componentDidMount(){
        axios.get(`${server}api/blogsection/`).then(res=>{
            let product=[]
            for(var i=0;i<res.data.length;i++){
                if(localStorage.getItem('productname')===res.data[i].name){
                     product.push({name:i,value:res.data[i]})
                }
            }

            console.log(product)
            this.setState({
                Product:product
            })
            console.log(this.state.Product)

            
        })
    }

    Remove(event){
       
           localStorage.removeItem('cart')
           window.location.reload()
       
    }

    MethodNext=()=>{
        this.props.methodNext()
    }

    MethodPrev=()=>{
        this.props.methodPrev()
    }


    MakeDecrease=()=>{

         
        setTimeout(()=>{
            if(Number(localStorage.getItem('cart'))<=1){
                
                localStorage.removeItem('cart')
                window.location.reload()
          }else{
             var v=Number(localStorage.getItem('cart'))
             v=v-1
             
             localStorage.setItem('cart',v)
             window.location.reload()
          }
        },500)
         
    }

    MakeIncrease=()=>{
        setTimeout(()=>{

            if(Number(localStorage.getItem('cart'))>=0){
                var value=Number(localStorage.getItem('cart'))
                  value=value+1
                  localStorage.setItem('cart',value)
                  window.location.reload()
            }

        },500)
      
     
    }


    ProductClick(v){

        this.setState({
            productclick:true,
            productname:v
        })

    }


    render(){
        const {data,values}=this.props;
        const cart = this.state.cartValue;
     
        return(
            <React.Fragment>

                {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}

                <div className='demo container maketwopart col-9 ' style={{zIndex:'20'}}>

            <div className=' productsection col-8'>
           
             {data?'':
             <div style={{display:'flex',flexDirection:'row',justifyContent:'space-between'}}>
             <h5><b>My Cart({localStorage.getItem('cart')?localStorage.getItem('cart'):'0'})</b></h5>
             </div>
             }
         
             
             <hr/>
             {localStorage.getItem('cart')?
             this.state.Product.map((item)=>
             <div key={item}>
             <div>
             <ul className='alg'>
               <li><img onClick={()=>this.ProductClick(item.value.name)} src={item.value.image1} alt=''  style={{height:'5rem',width:'5rem',cursor:'pointer'}}/>
                
               </li>
               <li> <div className='value' >
                        <input className='circleinput' onClick={this.MakeDecrease} type='submit' value="-"/>
                        <input className='circleinput' type='text' value={localStorage.getItem('cart')} />
                        <input className='circleinput' onClick={this.MakeIncrease} type='submit' value="+"/>
                        <p className='mrp'> <i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</p>
                    </div>
                    </li>
               <li>
                   <ul>
                   <li><b>{item.value.name}</b></li>
                   {data? <li><b><h6><i style={{color:'blue'}} class="fa fa-map-marker" aria-hidden="true"></i>  Deliver to {values.pin}</h6></b></li>:''}
                   <li><b>Size:</b> {item.value.size}</li>
                   <li><b>Flavour:</b> Chocolate flavour</li>
                   <li className='remove' ><a onClick={this.Remove}><b><i class="fa fa-trash" aria-hidden="true"></i></b> </a></li>
                   </ul>
                     
                     
               </li>
           </ul>
           <hr/>
           </div>


           </div>


            
           
             ):<div>No Cart item</div>

            }
            
           

           {data?
           
           <div style={{textAlign:'right'}}>
            
            <Button style={{backgroundColor:'rgb(209, 79, 18)',color:'white'}} onClick={this.MethodPrev} >Previous</Button>
            <Button onClick={this.MethodNext}  style={{backgroundColor:'rgba(0, 255, 255, 0.637)',margin:'3px'}} >Continue</Button>
            <p style={{marginTop:'3px',color:'green',fontWeight:'bold'}}>Step 2/4</p>
          </div>
           
           :
           <div style={{textAlign:'right'}}>
           <Link to='/checkout'>{localStorage.getItem('cart')? <button  className='btn btn-warning'>Checkout</button>:''}</Link>
           </div> 
           
           }


           
             
                
            </div>



            {this.state.Product.map((item)=>{
                if(localStorage.getItem('cart')){

                    return(

                        <div className='pricedetails ' key={item.name}>
                        <h4 style={{padding:'2px'}}><b>Price details</b></h4>
                        
                      <div>        
                        <table className='table table-primary'>
                            <thead>
                                <tr>
                                    <td><b>Price({localStorage.getItem('cart')})</b></td>
                                    <td><b>Discount</b></td>
                                    <td><b>Delivery charges</b></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style={{textAlign:'center'}}>{Number(item.value.price)} x{Number(localStorage.getItem('cart'))}</td>
                                    <td style={{textAlign:'center'}}>{Number(item.value.offer)}x{Number(localStorage.getItem('cart'))}</td>
                                    <td style={{textAlign:'center'}}>50</td>
                                </tr>
                            </tbody>
                        </table>
        
                      </div>
                        
        
                        <hr/>
        
                        <h5 style={{textAlign:'center'}}><b>Total amount </b>= {Number(item.value.price)*Number(localStorage.getItem('cart'))-Number(item.value.offer)*Number(localStorage.getItem('cart'))+50}</h5>
                      </div>
                    )
                }
               
              
                
            })}


             </div>

            </React.Fragment>
            
        )
    }
}

export default Cart;