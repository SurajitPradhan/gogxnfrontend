import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import '../Footer/style.css'
import axios from 'axios'
import server from '../server/server'

export class Footer extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            name:'',
            email:""
             
        }
        this.Buttonsubmit=this.Buttonsubmit.bind(this)
        this.InputChange=this.InputChange.bind(this)
    }
    
    componentDidMount=()=>{

      }

      InputChange(e){
             this.setState({
                    [e.target.name]:e.target.value
             })
      }

      Buttonsubmit(){
            axios.post(`${server}api/Subscribeus/`,{
                name:this.state.name,
                email:this.state.email
            })
            .then(res=>{
                console.log()
                
            })
      }

    render() {
        return (
             <div className="footers" >
            <div className="footer-top px-3">
                <div className="footer_container px-5">
                    <div className="row">
                        <div className="col-md-3 col-sm-6 colxs-12 segment-one">
                            <ul>
                           <Link to='/trackorder'>  <li className="tracing" style={{color:"red"}}><b>TRACK ORDER</b></li></Link>
                           <Link to='/becomedealer' ><li className="distributors" style={{color:"red"}}><b>GET DISTRIBUTORSHIP</b></li></Link>  
                           <Link to='/frequentlyaskquestions'> <li>FAQs</li></Link>
                           
                            
                            <Link to="/carrer"><li>Careers</li></Link>  
                          
                            </ul>
                        </div>

                        <div className="col-md-3 col-sm-6 colxs-12 segment-two">
                         
                            <ul>
                           
                           <Link to='/terms&condition'> <li>Terms & Conditions</li></Link> 
                           <Link to='/privacy&policy'><li>Privacy & Policy</li></Link>  
                           <Link to='/refund&policy'><li>Refunds & Cancellations</li></Link> 
                      
                          </ul>


                        </div>




                        <div className="col-md-3 col-sm-6 colxs-12 segment-three">

                            <h4 className="about mt-2" style={{fontFamily: "Roboto,Arial,sans-serif"}}>GET SOCIAL</h4>
                         
                         <div style={{display:'flex',flexDirection:'row',}} >

                         <a style={{display:'flex',alignItems:'center',justifyContent:'center'}}  className="icon " href="https://www.facebook.com/gogxn/" target="_blank"><i style={{color:'white'}} class="fa fa-facebook fa-1.5x"></i></a>&nbsp;
                            <a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://twitter.com/go_gxn" target="_blank"><i style={{color:'white'}} className="fa fa-twitter fa-1.5x"></i></a>&nbsp;
                            <a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://www.flickr.com/people/158004145@N04/" target="_blank"><i style={{color:'white'}} className="fa fa-flickr fa-1.5x"></i></a>&nbsp;
                            <a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://in.pinterest.com/go_gxn/" target="_blank"><i style={{color:'white'}} className="fa fa-pinterest fa-1.5x"></i></a>&nbsp;
                            <a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://www.youtube.com/channel/UCa4cEu-G1AejDb27q66L5ag" target="_blank"><i style={{color:'white'}} className="fa fa-youtube fa-1.5x"></i></a>
                         </div>
                         
                        
                        </div>
                     


                        <div className="col-md-3 col-sm-6 colxs-12 segment-four">
                            <h4 className="about mt-2" style={{fontFamily: "Roboto,Arial,sans-serif"}}>SUBSCRIBE US</h4>
                            <form action="">
                            <input type="text" name='name' value={this.state.name} onChange={this.InputChange} class="form-control " id="inputnamel4" placeholder="Name"/>
                            <input type="email" name='email' value={this.state.email} onChange={this.InputChange} class="form-control mt-2" id="inputEmail4" placeholder="Email"/>
                            <button type="button" onClick={this.Buttonsubmit} class="btn btn-primary mt-2">Subscribe</button>
                            </form> 

                        </div>
                    </div>
                </div>
            </div>
</div>   
        )
    }
}

export default Footer



