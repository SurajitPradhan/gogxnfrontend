import React, { Component } from 'react'
import './style.css'
export class RefundPolicy extends Component {
    render() {
        return (
            <div className="policies ">
                <h1 className="policy_tag py-4 " >
                REFUNDS & CANCELLATIONS
                </h1>
                <p className="instst"><a href="#">Gogxn.com</a> left no stone unturned that user will have to refund the purchased product but if any situation leads to these 
                circumstances then we would gladly process the required steps in order to provide user flexibility.</p >
                <p className="instruction pt-2">Read the following guideline carefully before initiating cancellation or refunding the product.</p>
                <ul className='unorderedlist pl-4 py-3 pb-5'>
                    <li className="instruction_list ">Until your product is not out for delivery you can cancel the order and no money will be charged.</li>
                    <li className="instruction_list">You are only eligible to refund the product within 10 days of successful delivery of the product. Please ensure that product should remain 
                        in the same state as you received such as packaging, box seal, material, and tag.</li>
                    <li className="instruction_list">The process of the refund will initiate after the verification of product authenticity in terms of quality and quantity.</li>
                    <li className="instruction_list">If your product is found damaged in any condition such as Box compression, scratch, came in water/fire contact then you are
                         not allowed to refund or claim.</li>
                    <li className="instruction_list">Shipping costs are non-refundable.</li>
                    <li className="instruction_list">The reason for rejection should be valid enough to process the refund.*</li>
                    <li className="instruction_list ">You can contact us at email- <a href="#">support@gogxn.com</a> or dial <b>+91-6366783141</b> to know more about refund policies.</li>
                  
                    </ul>         
            </div>
        )
    }
}

export default RefundPolicy
