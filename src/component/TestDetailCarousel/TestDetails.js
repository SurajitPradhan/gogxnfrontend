import React from 'react';
import ReactDOM from 'react-dom';
// import './style.css';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import axios from 'axios'
import Card from '../Card';
import { Redirect } from 'react-router';
import NewArrivalCard from '../NewArrivalCard';
import TestDetailCarousel from '.';
import server from '../server/server';

// import Card from './Card';
const options = {
  margin: -10,
 margin: 15,  
  // padding: 50,
  responsiveClass: true,
 dots:true,

  autoplay: false,
 loop:true,
  smartSpeed: 1000,
  responsive: {
      0: {
          items: 1,
      },
      250:{
          items:2,
      },
      400: {
          items: 2,
      },
      600: {
          items: 3,
      },
      650: {
        items: 4,
    },
      700: {
          items: 3,
      },
      1000: {
          items: 5,
      }
  },
};

class TestDetails extends React.Component {
    constructor(props) {
        super(props)
    
        this.state =

         {
            name:[],
            price:[],
            image1:[],
            image2:[],
            cashback:[],
            discount:[],
            id:[]     
        }
       
    }
    
    componentDidMount=()=>{
        axios.get(`${server}api/carsouel/`)
        .then((res)=>{
          for (var i = 0; i < res.data.length; i++)
         
          this.setState({
            name: [...this.state.name, res.data[i].name],
            price: [...this.state.price, res.data[i].price],
            id: [...this.state.id, res.data[i].id],
            discount: [...this.state.discount, res.data[i].discount],
            cashback: [...this.state.cashback, res.data[i].cashback],
            image1: [...this.state.image1, res.data[i].image1],
            image2: [...this.state.image2, res.data[i].image2],
            // data:[res.data]
            productclik:false,
            productname:''
          })
console.log(res)
          this.ProductClick=this.ProductClick.bind(this)
    
         
        })
      }


      ProductClick(v){
           this.setState({
             productclik:true,
             productname:v
           })
      }

  render() {
    
       return (

         <React.Fragment>

        <div>
          {this.state.productclik?<Redirect to={{pathname:`/productpage/${this.state.productname}`, state:{id:this.state.productname}}} />:''}
        </div>
<div class="carousel px-4 "><center>


   
    <h3 className=" proteins pt-4 "> <b>You May Also Like</b> </h3>
  
  </center>



       
  <OwlCarousel className="slider-items owl-carousel owl-theme "  {...options}>

                      <div class="item" onClick={()=>this.ProductClick(this.state.name[0])} ><TestDetailCarousel name={this.state.name[0]} image1={this.state.image1[0]} image2={this.state.image2[0]} price={this.state.price[0]}  discount={this.state.discount[0]} cashback={this.state.cashback[0]} /></div>
                      <div class="item" onClick={()=>this.ProductClick(this.state.name[1])} ><TestDetailCarousel name={this.state.name[1]} image1={this.state.image1[1]}  image2={this.state.image2[1]} price={this.state.price[1]}  discount={this.state.discount[1]} cashback={this.state.cashback[1]}/></div>
                      <div class="item" onClick={()=>this.ProductClick(this.state.name[2])} ><TestDetailCarousel name={this.state.name[2]} image1={this.state.image1[2]}  image2={this.state.image2[2]} price={this.state.price[2]}  discount={this.state.discount[2]} cashback={this.state.cashback[2]}/></div>
                      <div class="item" onClick={()=>this.ProductClick(this.state.name[3])} ><TestDetailCarousel name={this.state.name[3]} image1={this.state.image1[3]}  image2={this.state.image2[3]} price={this.state.price[3]}  discount={this.state.discount[3]} cashback={this.state.cashback[3]}/></div>
                      <div class="item" onClick={()=>this.ProductClick(this.state.name[0])} ><TestDetailCarousel name={this.state.name[0 ]} image1={this.state.image1[0]}  image2={this.state.image2[0]} price={this.state.price[0]}  discount={this.state.discount[0]} cashback={this.state.cashback[0]}/></div>
                      <div class="item" onClick={()=>this.ProductClick(this.state.name[1])} ><TestDetailCarousel name={this.state.name[1]} image1={this.state.image1[1]}  image2={this.state.image2[1]} price={this.state.price[1]}  discount={this.state.discount[1]} cashback={this.state.cashback[1]}/></div>
                      <div class="item" onClick={()=>this.ProductClick(this.state.name[2])} ><TestDetailCarousel name={this.state.name[2]} image1={this.state.image1[2]}  image2={this.state.image2[2]} price={this.state.price[2]}  discount={this.state.discount[2]} cashback={this.state.cashback[2]}/></div>
                   

                                   
                  </OwlCarousel> 
</div>

</React.Fragment>
)   
};
}

export default TestDetails;


