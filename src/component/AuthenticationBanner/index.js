import React, { Component } from 'react'
import { Redirect } from 'react-router'
import authbanner from '../../images/authenticity.jpg'
import '../AuthenticationBanner/style.css'
export class AuthenticationBanner extends Component {

    constructor(){
        super()
        this.state={
            Authenticityclick:false
        }
        this.BannerClick=this.BannerClick.bind(this)
    }

    BannerClick(){

      this.setState({
          Authenticityclick:true
      })
       
    }


    render() {
        return (
            <React.Fragment>
                {this.state.Authenticityclick?<div><Redirect to='/authenticity' /></div>:''}
            <div className="authentication_banner ">
                 <img src={authbanner} onClick={this.BannerClick}  style={{cursor:'pointer'}} className="img-fluid bannerimage" alt="Responsive image"  width="100%"/>
            </div>
            </React.Fragment>
        )
    }
}

export default AuthenticationBanner;
