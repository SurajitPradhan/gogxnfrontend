import React, { Component } from 'react'
import './style.css'
export class Overview extends Component {
    render() {
        return (
            <div className="overview_section">
                <p className="overview_context ">We are amongst the India’s most trusted health supplement brand, with a mission to make nutrition more
                     reliable and effective. With industry-first step in various direction such as such as ensure scan validity, 
                     layers of authenticity, protein test certificates,  scientifically researched products, and more, GXN has
                      rapidly risen to become the consumer's favourite brand. We're based on getting you fitter and healthier 
                      using our experts' comprehensive analysis and 
                    experience of the ever-changing market. So make health a priority today and let us handle the rest!!!!</p>
            </div>
        )
    }
}

export default Overview
