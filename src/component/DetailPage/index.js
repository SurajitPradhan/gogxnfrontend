import { colors } from '@material-ui/core';
import axios from 'axios';
import React, { Component } from 'react'
import ProductAll from '../AllProducts/AllProducts';
// import DetailPageDes from '../DetailPageDes';
import Header from '../Header';
import {useEffect} from 'react'
import server from '../server/server';
import YouMayLk from '../YouMayLk'


import './style.css';
import { Redirect } from 'react-router';
import TestDetailDes from '../TestDetailDes';
import DetailPageDes from '../DetailPageDes';
import TestDetails from '../TestDetailCarousel/TestDetails';
import TestImageCarousel from '../TestImageCarousel';
// import DetailPageDes2 from '../DetailPagedDes2';

export class DetailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            heading: "GXN N.O. PUMP – BEST PRE WORKOUT",
            image:"https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
            Product:[],
            checkout:0,
            checkoutbuttonclick:false,
            addtocart:false,
            buynow:false,
            goforcart:false,
           
        }
        this.CheckoutClick=this.CheckoutClick.bind(this)
        this.GotoCartpage=this.GotoCartpage.bind(this)
    }

    componentDidMount(){
        axios.get(`${server}api/product/`).then(res=>{
            let product=[]
            if(this.props.location.state.id!=null){
            for(var i=0;i<res.data.length;i++){
                if(this.props.location.state.id==res.data[i].name){
                    product.push({name:i,value:res.data[i]})
                }
            }
        }

       
            this.setState({
                 Product:product
            })
        })
    }

    CheckoutClick=(v)=>{
    
        this.setState({
            addtocart:true
     })

     setTimeout(()=>{
         
        if(localStorage.getItem('cart')){
           var checkout=Number(localStorage.getItem('cart'))
            checkout=checkout+1
            localStorage.setItem('cart',checkout)
            localStorage.setItem('productname',v)
           
        }else{
           var checkout=0
            checkout=checkout+1
            localStorage.setItem('cart',checkout)
            localStorage.setItem('productname',v)
          
        }
        
        window.location.reload()
     },500)
   


    }


   GotoCartpage(){
      
       this.setState({
           goforcart:true
       })
   }


    render() {

        

        
        return (
            
            <React.Fragment>
                {this.state.goforcart?<Redirect to='/cartvalue' />:''}
                
                {
                    this.state.Product.map((item)=>{

                        return(
                            <div className="test_details px-3">
                            <div className="detail_row row">
                                <div className="columns col-md-7">
                                <div className=" boxes ">
                                       <div className="detail_image">
                                           <li className=" products_img_list mt-5">
                                               <img src={item.value.image1} className="img-fluid"/>
                                               <ul id="active" className="products_img_ullist">
                                               <img src={item.value.image1} className="img-fluid"/>
                                               </ul>
                                               </li>
               
                                           <li className="products_img_list">
                                               <img src={item.value.image2} className="img-fluid"/>
                                               <ul className="products_img_ullist">
                                               <img src={item.value.image2} className="img-fluid"/>
                                               </ul>
                                               </li>
                                           <li className="products_img_list">
                                               <img src={item.value.image2} className="img-fluid"/>
                                               <ul className="products_img_ullist">
                                               <img src={item.value.image2} className="img-fluid"/>
                                               </ul>
                                           </li>
                                           <li className="products_img_list">
                                               <img src={item.value.image3} className="img-fluid"/>
                                               <ul className="products_img_ullist">
                                               <img src={item.value.image3}/>
                                               </ul>
                                               </li>
                                           <li className="products_img_list">
                                               <img src={item.value.image4} className="img-fluid"/>
                                               <ul className="products_img_ullist">
                                               <img src={item.value.image4}/>
                                               </ul>
                                               </li>
               
                                    
                                       </div>
                                
                                     {/* <div className=""><DetailPageDes/></div> */}
                                   
                                   </div> 
{/* Carousel for mobile view */}
                                 <div className="testcarousel "> <TestImageCarousel/> </div> 

  {/* Buttons                                */}
                                   <div  className="butto row px-5 col-md-block " >
               <div class="buttons col clo-md-6"  style={{marginLeft:"20px", marginRight:"20px" }}> 
               <button type="button" class="btn btn-primary" style={{padding:"20px", background:" #FF9F00",width:"120%"}}> <i class="fa fa-cart-plus " aria-hidden="true"></i> ADD TO CART</button>
                 </div>
                <div class="buttons col col-md-6" style={{margin:"20px"}}>
                <button type="button" class="btn btn-primary" style={{padding:"20px", width:"120%", background:"#ff5930"}}>  <i class="fa fa-bolt " aria-hidden="true"></i> BUY NOW</button>
                </div>
        
{/*Description section */}
               
              
              
               </div>
        

 {/* Desciription        */}
               <div className="destils d-md-block d-none mt-4 px-4"><DetailPageDes data={item.value.name}/></div>
                         </div>
                                <div className="columns col-md-5">
                                    <div className="total_des">
                                    {/* Heading */}
                                    <p className="name_product mt-4"><b>{item.value.name}</b></p>
                                 {/* Rating */}
                                 <div className="ratings_pro ">
                                   <i class="fa fa-star" aria-hidden="true" style={{color:"goldenrod"}}></i>
                                   <i class="fa fa-star" aria-hidden="true" style={{color:"goldenrod"}}></i>
                                   <i class="fa fa-star" aria-hidden="true" style={{color:"goldenrod"}}></i>
                                   <i class="fa fa-star" aria-hidden="true" style={{color:"goldenrod"}}></i>
                                   <i class="fa fa-star-half-o" aria-hidden="true" style={{color:"goldenrod"}}></i>
                                   ({item.value.rating})
                                   </div>
                        
                                
                                <b> <hr/></b>   
                                    {/* short description about product */}
                                    <div className="pro_short_des px-5">
                                       
                                    { <div style={{fontSize:"12px", fontFamily:"Roboto,Arial,sans-serif",  marginLeft:"-26px", lineHeight:"25px"}} dangerouslySetInnerHTML={{ __html: item.value.shortdescription }} /> }

                       
                                    </div>
                                    <b><hr/></b>
                                    {/* flavor and size section */}
                                    {/* sizes */}
                                    <div className="size_des">
                                    <p className="size_description">Size:</p>
                                    
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3 "><b>30 Serving</b></button>
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3"><b>60 Serving</b></button>
                                    </div>
               {/* Flavors */}
                                    <div className="flavor_des mt-3">
                                    <p className="flavor_description">Flavor:</p>
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3 "><b>Watermelon</b></button>
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3"><b>Fruit Punch</b></button>
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3 "><b>BlueBerry</b></button>
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3"><b>Guava</b></button>
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3 "><b>Pomegranate</b></button>
                                    <button type="button" class="btn btn-outline-danger mt-2 mr-3"><b>Tropical Fruit</b></button>
                                    </div>
               
               <b><hr/></b>
               
               {/* MRP SECTION */}
                                   <div><h6 className="offer_price_des"><b>MRP:</b>                       
                                   &nbsp;<i className="fa fa-inr"> {item.value.price}</i></h6></div>
               
               {/* OFFER SECTION */}
                                   
                                   
                                   <div><h6 className="offer_des"><b>Offer Price:</b>                      
                                   &nbsp;<i className="fa fa-inr fa-x" style={{color:"red"}}> {item.value.price}</i><p className="inclusives_des">inclusive of all taxes </p></h6>   </div> 
                                             
               {/* HOW MUCH YOU SAVED */}
               
                                   <div><h6 className="saving_des"><b>You Save:</b>                      
                                   &nbsp;<i className="fa fa-inr fa-x" style={{color:"red"}}> {item.value.price}</i> <p className="inclusives_des"> inclusive of all taxes </p></h6>   </div> 
               

                                   <div  className="buttos  col-md-block " >
               <div class="buttons  "  style={{ }}> 
               <button type="button" class="btn btn-primary" style={ {paddingTop:"10px", paddingBottom:"10px",width: "100%",background:" #FF9F00"}}> <i class="fa fa-cart-plus " aria-hidden="true"></i> ADD TO CART</button>
                 </div>
                <div class="buttons  " style={{}}>
                <button type="button" class="btn btn-primary" style={{paddingTop:"10px", paddingBottom:"10px", width: "100%", background:"#ff5930"}}>  <i class="fa fa-bolt " aria-hidden="true"></i> BUY NOW</button>
                </div>
        
{/*Description section */}
               
              
              
               </div>
        
               {/* COUPON SECTION */}
               
                                   <div class = "radio mt-3">
                                   <label>
                                   <input type="checkbox" id=""/> 
                                   &nbsp;  <i class="fa fa-tag" aria-hidden="true"  style={{color:"red"}} >  </i>  Apply Coupon
                                   <strong className="apply_coupon_des"> NOPRE5</strong> | Upto 5% off
                                   </label></div>
               
               {/* invantory status */}
                 
                                   <div className="product_invantory_des mt-2">
                                   {/* <strong className="stocks_des">In Stock</strong> -  Usually dispatched within 24 hours */}
                                   <strong className="out_stocks_des">Out of Stock</strong> - Kindly contact customer care at +91-8447587080
                                   </div>
               
               {/* product type */}
                            
                                  <div className="mark_des mt-3" > <i class="fa fa-circle" aria-hidden="true" 
                                  style={{color:"green",border:"2px solid green", width:"20px", alignItems:"left"}}></i> This is a
                                  <strong className="vegmark_des" 
                                  style={{fontWeight:"800px", fontFamily:"Roboto,Arial,sans-serif"}}>Vegetarian</strong> Product
                                  </div>
               
               {/* Add to wish list section */}
               
                                   <div className="carry_wish_des">
                                   <button type="button" class="btn button_des " style={{marginLeft:"-18px"}} >
                                   <a href="#"  > <i class="fa fa-heart-o" aria-hidden="true"> Add to Your WishList</i></a> 
                                   </button>
                                   </div> 
                                   <div className="destils d-md-none mt-3"><TestDetailDes data={item.value.name}/></div>
                                </div>
                                </div>
                            </div>
                         <div className=""><TestDetails /></div>

                        </div>
                        
                    )})
                }

          
            </React.Fragment>
       
       
       
       
       )
    }
}

export default DetailPage;

