import axios from 'axios';
import React ,{Component} from 'react'
import { Redirect } from 'react-router';
import logo4 from  '../../images/logo4.png'
import '../signup/style.css'
import server from '../server/server';
class SignUp extends Component{
    constructor(){
        super();
        this.state={
            name:'',
            email:'',
            phone:'',
            password:'',
            confirmpassword:'',
            error:'',
            success:'',
            loading:false,
            redirect:false,
        }
        this.ButtonSubmit=this.ButtonSubmit.bind(this)
        this.InputChange=this.InputChange.bind(this)
        this.Makingredirect=this.Makingredirect.bind(this)
    }


    InputChange(event){
        this.setState({
          [event.target.name]:event.target.value
        })
    }


    Makingredirect(){
  
        setTimeout(()=>{
          this.setState({
            redirect:true
          })
        },2000)
      
      }


    ButtonSubmit(){
         this.setState({loading:true})
        if(!this.state['name']){
            this.setState({
                error:'Name Can not be null',
                success:'',
                loading:false
            })
            
          }
          else if(!this.state['email']){
              this.setState({
                  error:'Email Can not be null',
                  success:'',
                  loading:false
              })
          }
          else if(!this.state['phone']){
              this.setState({
                  error:"Phone Should not be null",
                  success:'',
                  loading:false
              })
          }else if(this.state.phone.length>10 | this.state.phone.length<10){
              this.setState({
                  error:'Number Should be 10 digit',
                  success:'',
                  loading:false
              })
          }else if(this.state.password.length<8){
              this.setState({
                  error:'Password length not less than 8',
                  success:'',
                  loading:false
              })
          }
          else if(!this.state['password']){
               this.setState({
                   error:'Password fill not be null',
                   success:'',
                   loading:false
               })
          }else if(!this.state['confirmpassword']){
              this.setState({
                  error:'Confirm password not be null',
                  success:'',
                  loading:false
              })
          }
          else if(this.state['password']!=this.state['confirmpassword']){
                this.setState({
                    error:'Confirm password should be matched with password',
                    success:'',
                    loading:false
                })
          }
          else{
    
        this.setState({loading:true})
         var isphone=false
         const temp =this.state.phone
         axios.get(`${server}api/contact/`).then(res=>{
              for(var i=0;i<res.data.length;i++){
                  if(temp==res.data[i].phone){
                      isphone=true
                      break;
                  }
              }
         })

        if(isphone){
            alert('account is already there');
        }else{
            axios.post(`${server}api/contact/`,{
                name:this.state.name,
                email:this.state.email,
                phone:this.state.phone,
                password:this.state.password
            })
            .then(res=>{
                 this.setState({
                     name:'',
                     email:'',
                     phone:'',
                     password:'',
                     confirmpassword:"",
                     error:'',
                     success:'Your account created successfully please wait for redirect..',
                     loading:true
                 })

                 this.Makingredirect();

            })

            
        }
        

          
      }
    }

    


    render(){
        return(

            <React.Fragment>
    <div style={{zIndex:'-1'}}>
    <div className='signinrender rounded border mb-3 text-center' style={{zIndex:'20'}}  >
  
    <img class="mb-4" src={logo4} alt="" width="180" height="51"/>
    <h1 class="h3 mb-3 fw-normal">Please create your account</h1>
    
    <div>
        {this.state.error?<div class="alert mx-auto col-7 alert-danger" role="alert">
   {this.state.error}
</div>:''}

{this.state.success?<div class="alert col-7 mx-auto alert-success" role="alert">
  {this.state.success}
</div>:''}

{this.state.loading?<div class="spinner-border text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>:''}

    </div>

    {this.state.redirect?<Redirect to='/home'/>:''}

    <input type="text" name='name' value={this.state.name} onChange={this.InputChange} class="row col-7 signup  mx-auto "  placeholder='Your Name' required autofocus/>
    <input type="email" name='email' value={this.state.email} onChange={this.InputChange}  class="row col-7 signup  mx-auto mt-2" placeholder="Email address" required autofocus/>
    
    <input type="number" name="phone" value={this.state.phone} onChange={this.InputChange} class="row col-7 signup  mx-auto mt-2" placeholder="Your phone" required autofocus/>
    <input type="password" name='password' value={this.state.password} onChange={this.InputChange}  class='row col-7 signup  mx-auto mt-2' placeholder="Passsword" required autofocus/>
    
    <input type="password" name='confirmpassword' value={this.state.confirmpassword} onChange={this.InputChange} class="row col-7 signup  mx-auto mt-2" placeholder="Confirm password" required/>

    <input class="row btn btn-lg btn-primary mx-auto mt-2" onClick={this.ButtonSubmit} type="submit" value={this.state.loading?'Creating..':'Create Your account'}/>
    <p class="mt-5 mb-3 text-muted">&copy; 2011-2021 Gxn.com</p>
  
     </div>
</div>
     </React.Fragment>
        )
    }
}

export default SignUp;