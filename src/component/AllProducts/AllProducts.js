import axios from 'axios';
import React, { Component } from 'react'
import { Redirect } from 'react-router';
import { withRouter} from 'react-router-dom';
import './style.css'
 class AllProduct extends Component {
         constructor(){
             super();
             this.state={
                   allProducts:[],
                   productClick:false,
                   productname:'',
             }
             this.ProductClick=this.ProductClick.bind(this)
         }

         componentDidMount(){
             axios.get('http://65.0.249.137:8000/api/product/').then((res)=>{
                         let Products=[]

                         for(var i=0;i<res.data.length;i++){
                                Products.push({name:i,value:res.data[i]})
                         }
                         Products.sort((a,b)=>(a.value.price>b.value.price?1:-1))
                         this.setState({
                             allProducts:Products
                         })
             })
         }

         ProductClick(v){

                 this.setState({
                     productClick:true,
                     productname:v
                 })
         }



    render() {
        return (
            <React.Fragment>

{/* {this.state.productClick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''} */}

                
{/* {this.state.allProducts.map( (item)=>
 
           
                <div key={item} onClick={()=>this.ProductClick(item.value.name)} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                               <div className="product_data_image col-sm-4 " style={{display:'flex',alignItems:'center',justifyContent:'center'}} >
                       <img src={item.value.image1} style={{width:'200px', height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
              
                       
    
                           </div> 

                           {console.log(typeof(item.value.price))}
                           <div className="col-sm-5">   
                     <h5><span className='ProductName'>{item.value.name}</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                       <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                        </div>

                    
                         <div className='col-sm-3'>
                        
                        <p><b><i className='fa fa-inr' aria-hidden='true'></i>{(Number(item.value.price)-Number(item.value.price)*Number(item.value.offer)/100)}</b></p>
                        <p><s><i class="fa fa-inr" aria-hidden="true"></i>{Number(item.value.price)}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                         <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {Number(item.value.price)*Number(item.value.offer)/100}</i>  </p>
                        

                          </div>
             
                      
                         </div>
                      
                      
                      
                      
                     */}
                           <div class="container bcontent ">
             {this.state.productClick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
             {this.state.allProducts.map( (item)=>
             <div class="cards">
                 <div class="row no-gutters" key={item} onClick={()=>this.ProductClick(item.value.name)}>
                     <div class="col-sm-5 "><center>
                         <img class="img-fluid" src="https://gogxn.com/wp-content/uploads/2020/12/ArmourWhey_2lbs_Chocolate.png"  width="200px" height="200px" alt="Suresh Dasari Card" cursor="pointer"/>
                         </center></div>
                     <div class="col-sm-7  ">
                         <div class="card-bodies">
                         {console.log(typeof(item.value.price))}
                             <p class="card-titlle" cursor='pointer'><b>{item.value.name}</b></p>
                             <p class="small_des d-lg-block d-none" style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>  
                             <div className="ratings d-md-none d-block ">
            <i class="fa fa-star" aria-hidden="true" style={{color:"goldenrod", fontSize:"10px"}}></i>
            <i class="fa fa-star" aria-hidden="true"  style={{color:"goldenrod", fontSize:"10px"}}></i>
            <i class="fa fa-star" aria-hidden="true"  style={{color:"goldenrod", fontSize:"10px"}}></i>
            <i class="fa fa-star" aria-hidden="true"  style={{color:"goldenrod", fontSize:"10px"}}></i>
            <i class="fa fa-star-half-o" aria-hidden="true"  style={{color:"goldenrod", fontSize:"10px"}}></i>
          
           {/* <h6 className="review_s d-md-none ">(8 customer review)</h6>  */}
            </div>
        
                               
                             <p class="card-texst-price"><del><i class="fa fa-inr" aria-hidden="true"></i> <b>{item.value.MRP}</b> </del> &nbsp;- &nbsp;
                       <b><i className='fa fa-inr' aria-hidden='true' style={{fontSize:"14px"}}></i>{(Number(item.value.MRP)-Number(item.value.MRP)*Number(item.value.Regular_Discount)/100)}</b>
                    
                        {/* <s><i class="fa fa-inr" aria-hidden="true"></i>{Number(item.value.MRP)}</s>  */}
                        <span style={{color:'green',fontWeight:'bold', fontSize:"10px"}}> ({item.value.Regular_Discount}<i class="fa fa-percent" aria-hidden="true"></i> off)</span></p>
                         <p><b>Save:</b><b> <i class="fa fa-inr" aria-hidden="true" style={{color:"red", fontSize:"12px"}}> {Number(item.value.MRP)*Number(item.value.Regular_Discount)/100}</i>  </b></p>
                         <p class="card-texst-rating" style={{ fontSize:"12px"}}>Save extra with Cashback</p>
                         <p class="card-texst-rating">Apply Coupon: </p>
                           
                             {/* <p class="card-texst-savedprice">Save:1234</p> */}

                             
                             {/* <a href="#" class="btn btn-primary">View Profile</a> */}
                         </div>
                     </div>
                 </div>
             </div>  )  }
      </div>
       


                
            </React.Fragment>
        )
    }
}

export default withRouter(AllProduct)

