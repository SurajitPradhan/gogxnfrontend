import React, { Component } from 'react'
import './style.css'
import axios from 'axios'
import ProductAll from './AllProducts'
import { Switch } from 'react-router';
import PopularityProduct from './Popularity';
import { withRouter} from 'react-router-dom'
import DiscountComponent from './DiscountComponent';
import FlavourPage from './Flavoursection'
import RatingSection from './Rating';

import LowToHigh from './LowToHigh';
import HighToLow from './HighToLow';

import SizeSection from './size';
import { Checkbox } from '@material-ui/core';
import Multifilter from './multifilter';




 class AllProducts extends Component {
        constructor(){
            super();
            this.state={
                value:[1,2,3],
                product:[],
                popularity:false,
                lowtohigh:false,
                ProductFilter:0,
                productSection:false,
                pricesection:false,
                pricevalue:'',
                sizesection:false,
                flavoursection:false,
                discountsection:false,
                filterPageHeight:false,
                discountPercentageValue:0,
                choiceTheFlavourSection:'',
                begginersection:false,
                beginervalue:'',
                sizeValue:0,
                checkboxvalue:0,
                checked:false,
                
            }
               
            this.Popularity=this.Popularity.bind(this)
            this.LowToHigh=this.LowToHigh.bind(this)
            this.HighToLow=this.HighToLow.bind(this)
            this.AllProduct=this.AllProduct.bind(this)
            this.ProductSectionClick=this.ProductSectionClick.bind(this)
            this.PriceSection=this.PriceSection.bind(this)
            this.SizeSection=this.SizeSection.bind(this)
            this.FlavourSection=this.FlavourSection.bind(this)
            this.DiscountSection=this.DiscountSection.bind(this)
            this.DiscountSection=this.DiscountSection.bind(this)
            this.DiscountSectionComponent=this.DiscountSectionComponent.bind(this)
            this.BeginerSection=this.BeginerSection.bind(this)
            this.BeginerSectionPage=this.BeginerSectionPage.bind(this)
            this.SizeChange=this.SizeChange.bind(this)
            this.CheckBoxSelect=this.CheckBoxSelect.bind(this)
        }

        componentDidMount(){
            axios.get('http://65.0.249.137:8000/api/product/').then(res=>{
            
                let Details = []
                for(var i=0;i<res.data.length;i++){
                    Details.push({name:i,value:res.data[i]})
                }
                
                this.setState({
                    product:Details
                })
            })

            
        }

        AllProduct(){
            this.setState({
                ProductFilter:0
            })
        }
       

        Popularity(){
               this.setState({
                   ProductFilter:1
               })
               
        }

        LowToHigh(){

            
            this.setState({
                ProductFilter:2,
            
            })
          

  
        }
        
        HighToLow(){
              this.setState({
                  ProductFilter:3,
           
              })
        }
        
    
    ProductSectionClick(){

        this.setState({
            productSection:!this.state.productSection,
            filterPageHeight:!this.state.filterPageHeight
        })
    }
   

    PriceSection(){
        this.setState({
            pricesection:!this.state.pricesection,
            filterPageHeight:!this.state.filterPageHeight
        })
    }

    SizeSection(){
        this.setState({
            sizesection:!this.state.sizesection,
            filterPageHeight:!this.state.filterPageHeight
        })
    }

    FlavourSection(){
        this.setState({
            flavoursection:!this.state.flavoursection,
            filterPageHeight:!this.state.filterPageHeight
        })
    }

  DiscountSection(){
        this.setState({
            discountsection:!this.state.discountsection,
            filterPageHeight:!this.state.filterPageHeight
        })
  }

  DiscountSectionComponent=(v)=>{
       this.setState({
           ProductFilter:4,
           discountPercentageValue:v
       })
    
  }

  MakeFlavourSection=(v)=>{
      this.setState({
          ProductFilter:5,
          choiceTheFlavourSection:v
      })

  }

  BeginerSection=()=>{
      this.setState({
          begginersection:!this.state.begginersection
      })
  }

  BeginerSectionPage=(v)=>{
    this.setState({
         beginervalue:v,
         ProductFilter:6
    })
  }

 SizeChange=(v)=>{
     this.setState({
         ProductFilter:7,
         sizeValue:v
     })

 }

 CheckBoxSelect(){
     const v=this.state.checkboxvalue
    this.setState({
           checkboxvalue:v+1,
           checked:!this.state.checked
    })
 }




    render() {
       
        const project = (value) => {
            switch(value) {
      
              case 0:   return <ProductAll />;
              case 1:   return  <PopularityProduct/>;
              case 2:   return  <LowToHigh/>;
              case 3:   return  <HighToLow/>
              case 4:   return  <DiscountComponent data={this.state.discountPercentageValue}/>
              case 5:   return  <FlavourPage data={this.state.choiceTheFlavourSection}/>
              case 6:   return  <RatingSection data={this.state.ratingValue}/>
              case 7:   return  <SizeSection data={this.state.sizeValue}/>
              case 8:   return <Multifilter />
              default:  return <h1>No Result Found</h1>
            }
          }
         
        console.log(this.state.checkboxvalue)
        return (
            <React.Fragment>
                <div  style={{display:'flex',flexDirection:'row',justifyContent:'space-between',width:'100%'}} >

                  <div  className='allproduct'>
                   
                   <ul>
                       <h5><b>FILTERS</b></h5>
                       <li style={{height:this.state.productSection?'auto':'15px'}} >
                           <h6 onClick={this.ProductSectionClick} style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}><b>PRODUCT TYPES</b>  <i class={this.state.productSection?"fa fa-angle-up":"fa fa-angle-down"  } aria-hidden="true"></i> </h6>
                           <ul style={{visibility:this.state.productSection?'visible':'hidden'}}>
                               <li className="subfilter" onClick={this.AllProduct}>   <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> Health Supplements </li>
                               <li className="subfilter">    <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> Mass Gainers</li>
                               <li className="subfilter">   <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> Whey Protien</li>
                               <li className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/>  Pre-Workout</li>
                               <li className="subfilter">   <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> Wellness Products</li>
                           </ul>
                       
                       </li>
                      <hr style={{backgroundColor:'red'}}/>
                       

                       <li style={{height:this.state.pricesection?'auto':'15px'}}>
                          <h6 onClick={this.PriceSection} style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}><b>PRICE</b> <i class={this.state.pricesection?"fa fa-angle-up":"fa fa-angle-down"  } aria-hidden="true"></i> </h6>
                           <ul  className="Pricetransition" style={{visibility:this.state.pricesection?'visible':'hidden'}}>
                               <li onClick={this.LowToHigh} className="subfilter">  <input class="form-check-input " checked={this.state.checked?'true':'false'} onClick={this.CheckBoxSelect} type="checkbox" value="" aria-label="Checkbox for following text input"></input>Low To High </li>
                               <li onClick={this.HighToLow} className="subfilter">  <input class="form-check-input " onClick={this.CheckBoxSelect} type="checkbox" value="" aria-label="Checkbox for following text input"></input> High to Low</li>
                           </ul>
                       </li>

                         <hr style={{backgroundColor:'red'}}/>
                       <li  style={{height:this.state.sizesection?'auto':'15px'}}>
                       <h6 onClick={this.SizeSection} style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}><b>SIZE</b> <i class={this.state.sizesection?"fa fa-angle-up":"fa fa-angle-down"  } aria-hidden="true"></i> </h6>
                                <ul style={{visibility:this.state.sizesection?'visible':'hidden'}}>
                                       <li onClick={()=>this.SizeChange(0)} className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"></input> 0-500 g</li>
                                       <li onClick={()=>this.SizeChange(1)} className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"></input> 500-1 kg</li>
                                       <li onClick={()=>this.SizeChange(2)} className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"></input> 1-2 kg</li>
                                       <li onClick={()=>this.SizeChange(3)} className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"></input> 2-3 kg</li>
                                       <li onClick={()=>this.SizeChange(4)} className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"></input> 3-4 kg</li>
                                       <li onClick={()=>this.SizeChange(5)} className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"></input> 4-5 kg </li>
                                </ul>
                       </li>

                       <hr style={{backgroundColor:'red'}}/>

                       <li style={{height:this.state.flavoursection?'auto':'15px'}}>
                       <h6 onClick={this.FlavourSection} style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}><b>FLAVOUR</b> <i class={this.state.flavoursection?"fa fa-angle-up":"fa fa-angle-down"  } aria-hidden="true"></i> </h6>
                      
                           <ul style={{visibility:this.state.flavoursection?'visible':'hidden'}}>
                               <li className="subfilter" onClick={()=>this.MakeFlavourSection('Chocolate')} >     <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> Chocolate</li>
                               <li className="subfilter" onClick={()=>this.MakeFlavourSection('Banana')} >   <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/>Banana</li>
                               <li className="subfilter" onClick={()=>this.MakeFlavourSection('Mango')} >     <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> Mango</li>
                               <li className="subfilter" onClick={()=>this.MakeFlavourSection('Strawberry')} >     <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> Strawberry</li>
                           </ul>

                       </li>

                       <hr style={{backgroundColor:'red'}}/>

                       <li style={{height:this.state.discountsection?'auto':'15px'}}>
                      
                       <h6 onClick={this.DiscountSection} style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}><b>DISCOUNT</b> <i class={this.state.discountsection?"fa fa-angle-up":"fa fa-angle-down"  } aria-hidden="true"></i> </h6>
                           <ul style={{visibility:this.state.discountsection?'visible':'hidden',position:'relative'}}>
                               <li onClick={()=>this.DiscountSectionComponent(40)} className="subfilter">    <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/>  More than 40%</li>
                               <li  onClick={()=>this.DiscountSectionComponent(30)}  className="subfilter">  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/>  More than 30%</li>
                               <li  onClick={()=>this.DiscountSectionComponent(20)} className="subfilter">   <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> More than 20%</li>
                               <li  onClick={()=>this.DiscountSectionComponent(10)} className="subfilter">   <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> More than 10%</li>
                           </ul>

                       </li>

                       <hr style={{backgroundColor:'red'}}/>

                      
                       <li style={{height:this.state.begginersection?'auto':'15px'}}>
                      
                       <h6 onClick={this.BeginerSection} style={{display:'flex',flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}><b>BEGINEER'S CHOICE </b> <i class={this.state.begginersection?"fa fa-angle-up":"fa fa-angle-down"  } aria-hidden="true"></i> </h6>
                       <div style={{visibility:this.state.begginersection?'visible':'hidden',height:'auto'}}>
                           <ul >
                                   <li onClick={()=>this.BeginerSectionPage(2)}>   <input class="form-check-input " type="checkbox" value="" aria-label="true"/> (2-3)  <i style={{color:'gold'}} className='fa fa-star' aria-hidden='true'></i></li>
                                   <li onClick={()=>this.BeginerSectionPage(3)}>  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> (3-4) <i style={{color:'gold'}} className='fa fa-star' aria-hidden='true'></i></li>
                                   <li onClick={()=>this.BeginerSectionPage(4)}>  <input class="form-check-input " type="checkbox" value="" aria-label="Checkbox for following text input"/> (4-5) <i style={{color:'gold'}} className='fa fa-star' aria-hidden='true'></i></li>
                            </ul>
                       </div>
                 
                       </li>
                      
                   </ul> 
                  </div>


                  
               
                   

                  <div className=' allproductpage p-3'>

                    
                     {this.state.checkboxvalue>1?project(8) :project(this.state.ProductFilter) }            
                  </div>

                </div>

             
            </React.Fragment>
        )
    }
}
export default AllProducts