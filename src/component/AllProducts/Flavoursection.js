import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router'

export default class FlavourPage extends Component {

     constructor(){
         super()
         this.state={
             allProducts:[],
             productname:'',
             productclick:false,
         }
     }

     ProductClick=(v)=>{
        this.setState({
            productclick:true,
            productname:v
        })
   }

     componentDidMount(){
        axios.get('http://65.0.249.137:8000/api/product/').then((res)=>{
                    let Products=[]

                    for(var i=0;i<res.data.length;i++){
                           Products.push({name:i,value:res.data[i]})
                    }
                
                    Products.sort((a,b)=>(a.value.price>b.value.price?1:-1))
                  
                    this.setState({
                        allProducts:Products,
                    })
        })
    }

    render() {
        const {data}=this.props

        switch (data) {
            case 'Chocolate': return (<div>
                 {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
                {
                    this.state.allProducts.map((item)=>{
                        var v=item.value.flavourType
                         var f=v.split(" ")
                         console.log(f.length)

                         if(f.length>1){
                             for(var i=0;i<f.length;i++){
                                 if(f[i]==='Chocolate'){
                                     return (
                                          <div onClick={()=>this.ProductClick(item.value.name)}  key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                        <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                       <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-imges-top" alt="not load"/> 
              
                  
                           </div> 
   
                      
   
                       <div className="col-5">   
                     <h5><span className='ProductName'>{item.value.name} Chocolate Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                       <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                        </div>
   
   
                    
                         <div className='col-3'>
                        
                        <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                        <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                         <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                        
   
                          </div>
             
                      
                         </div>
                                )
                                 }
                             }
                         }else{
                             if(f[0]==='Chocolate'){
                               
                                 return(
                                    <div onClick={()=>this.ProductClick(item.value.name)}  key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                                    <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                   <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-imges-top" alt="not load"/> 
                          
                              
                                       </div> 
               
                                  
               
                                   <div className="col-5">   
                                 <h5><span className='ProductName'>{item.value.name} Chocolate Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                   <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                    </div>
               
               
                                
                                     <div className='col-3'>
                                    
                                    <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                    <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                     <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                    
                                      </div>
                         
                                  
                                     </div>
                                 )
                             }
                         }


                    })
                }

            </div>)
            case 'Banana':return (<div>
                 {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
                {
                    this.state.allProducts.map((item)=>{
                        var v=item.value.flavourType
                         var f=v.split(" ")
                         console.log(f.length)

                         if(f.length>1){
                             for(var i=0;i<f.length;i++){
                                 if(f[i]==='Banana'){
                                     return (
                                          <div onClick={()=>this.ProductClick(item.value.name)}  key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                        <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                       <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-imges-top" alt="not load"/> 
              
                  
                           </div> 
   
                      
   
                       <div className="col-5">   
                     <h5><span className='ProductName'>{item.value.name} Banana Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                       <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                        </div>
   
   
                    
                         <div className='col-3'>
                        
                        <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                        <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                         <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                        
   
                          </div>
             
                      
                         </div>
                                )
                                 }
                             }
                         }else{
                             if(f[0]==='Banana'){
                                 return(
                                    <div onClick={()=>this.ProductClick(item.value.name)}  key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                                    <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                   <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                          
                              
                                       </div> 
               
                                  
               
                                   <div className="col-5">   
                                 <h5><span className='ProductName'>{item.value.name} Banana Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                   <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                    </div>
               
               
                                
                                     <div className='col-3'>
                                    
                                    <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                    <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                     <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                    
                                      </div>
                         
                                  
                                     </div>
                                 )
                             }
                         }


                    })
                }

            </div>)
            case 'Mango':return (<div>
                 {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
                {
                    this.state.allProducts.map((item)=>{
                        var v=item.value.flavourType
                         var f=v.split(" ")
                         console.log(f.length)

                         if(f.length>1){
                             for(var i=0;i<f.length;i++){
                                 if(f[i]==='Mango'){
                                     return (
                                          <div key={item.name} onClick={()=>this.ProductClick(item.value.name)}  className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                        <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                       <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
              
                  
                           </div> 
   
                      
   
                       <div className="col-5">   
                     <h5><span className='ProductName'>{item.value.name} Mango Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                       <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                        </div>
   
   
                    
                         <div className='col-3'>
                        
                        <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                        <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                         <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                        
   
                          </div>
             
                      
                         </div>
                                )
                                 }
                             }
                         }else{
                             if(f[0]==='Mango'){
                                 return(
                                    <div key={item.name} onClick={()=>this.ProductClick(item.value.name)}  className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                                    <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                   <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                          
                              
                                       </div> 
               
                                  
               
                                   <div className="col-5">   
                                 <h5><span className='ProductName'>{item.value.name} Mango Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                   <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                    </div>
               
               
                                
                                     <div className='col-3'>
                                    
                                    <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                    <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                     <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                    
                                      </div>
                         
                                  
                                     </div>
                                 )
                             }
                         }


                    })
                }

            </div>)
            case 'Strawberry':return (<div>
                 {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
                {
                    this.state.allProducts.map((item)=>{
                        var v=item.value.flavourType
                         var f=v.split(" ")
                         console.log(f.length)

                         if(f.length>1){
                             for(var i=0;i<f.length;i++){
                                 if(f[i]==='Strawberry'){
                                     return (
                                          <div key={item.name} onClick={()=>this.ProductClick(item.value.name)}  className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                        <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                       <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
              
                  
                           </div> 
   
                      
   
                       <div className="col-5">   
                     <h5><span className='ProductName'>{item.value.name} Strawberry Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                       <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                        </div>
   
   
                    
                         <div className='col-3'>
                        
                        <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                        <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                         <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                        
   
                          </div>
             
                      
                         </div>
                                )
                                 }
                             }
                         }else{
                             if(f[0]==='Strawberry'){
                                 return(
                                    <div key={item.name} onClick={()=>this.ProductClick(item.value.name)}  className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                        
                                    <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                   <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                          
                              
                                       </div> 
               
                                  
               
                                   <div className="col-5">   
                                 <h5><span className='ProductName'>{item.value.name} Strawberry Flavour</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                   <p style={{marginTop:'3px',fontSize:'13px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                    </div>
               
               
                                
                                     <div className='col-3'>
                                    
                                    <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                    <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                     <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                    
                                      </div>
                         
                                  
                                     </div>
                                 )
                             }
                         }


                    })
                }

            </div>)
                
            default: return (<h3>this is the default section</h3>)
        }
    }
}
