import React, { Component } from 'react'
import axios from 'axios';
import { Redirect } from 'react-router'

export default class SizeSection extends Component {
          constructor(){
              super()
              this.state={
                  allProducts:[],
                  productclick:false,
                  productname:''
              }
              this.ProductClick=this.ProductClick.bind(this)
          }


          componentDidMount(){
            axios.get('http://65.0.249.137:8000/api/product/').then((res)=>{
                        let Products=[]
    
                        for(var i=0;i<res.data.length;i++){
                               Products.push({name:i,value:res.data[i]})
                        }
                    
                        Products.sort((a,b)=>(a.value.price>b.value.price?1:-1))
                      
                        this.setState({
                            allProducts:Products,
                        })
            })
        }


        ProductClick=(v)=>{
             this.setState({
                 productclick:true,
                 productname:v
             })
        }


    render() {
        const {data}=this.props

        switch (data) {
            case 0: return(<div>

                {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
                        
                {this.state.allProducts.map( (item)=>{
                     
                     var v=item.value.size.split(" ")
                     console.log(v)
                     if(v.length>1){
                        for(var i=0;i<v.length;i++){
                            console.log(v[i])
                            if(Number(v[i])<=0.5){
                                return (
                                    <div key={item.name} onClick={()=>this.ProductClick(item.value.name)} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                                            
                                            <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                           <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                                  
                                      
                                               </div> 
                    
                    
                    
                                           <div className="col-5">   
                                         <h5><span className='ProductName'>{item.value.name}({v[i]}kg)</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                           <p style={{marginTop:'3px',lineHeight:'20px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                            </div>
                    
                    
                                        
                                             <div className='col-3'>
                                            
                                            <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                            <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                             <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                            
                    
                                              </div>
                                 
                                          
                                             </div>
                                       )
                            }
                        }
                     }
                      
                    })
                           
                  
                  }
            </div>)
            case 1: return(<div>

{this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
                        
                {this.state.allProducts.map( (item)=>{
                     
                     var v=item.value.size.split(" ")
                     console.log(v)
                     if(v.length>1){
                        for(var i=0;i<v.length;i++){
                            console.log(v[i])
                            if(Number(v[i])>=0.5 && Number(v[i])<=1){
                                return (
                                    <div onClick={()=>this.ProductClick(item.value.name)} key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                                            
                                            <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                           <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                                  
                                      
                                               </div> 
                
                    
                    
                                           <div className="col-5">   
                                         <h5><span className='ProductName'>{item.value.name} ({v[i]}kg) </span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                           <p style={{marginTop:'3px',lineHeight:'20px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                            </div>
                    
                    
                                        
                                             <div className='col-3'>
                                            
                                            <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                            <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                             <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                            
                    
                                              </div>
                                 
                                          
                                             </div>
                                       )
                            }
                        }
                     }
                      
                    })
                           
                  
                  }
            </div>)
            case 2: return(<div>
                         {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}
                {this.state.allProducts.map( (item)=>{
                     
                     var v=item.value.size.split(" ")
                     console.log(v)
                     if(v.length>1){
                        for(var i=0;i<v.length;i++){
                            console.log(v[i])
                            if(Number(v[i])>=1 && Number(v[i])<=2){
                                return (
                                    <div onClick={()=>this.ProductClick(item.value.name)} key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                                            
                                            <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                           <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                                  
                                      
                                               </div> 
                    
                    
                    
                                           <div className="col-5">   
                                         <h5><span className='ProductName'>{item.value.name}({v[i]}kg)</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                           <p style={{marginTop:'3px',lineHeight:'20px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                            </div>
                    
                    
                                        
                                             <div className='col-3'>
                                            
                                            <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                            <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                             <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                            
                    
                                              </div>
                                 
                                          
                                             </div>
                                       )
                            }
                        }
                     }
                      
                    })
                           
                  
                  }
            </div>)
            case 3: return(<div>
                       {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}  
                {this.state.allProducts.map( (item)=>{
                     
                     var v=item.value.size.split(" ")
                     console.log(v)
                     if(v.length>1){
                        for(var i=0;i<v.length;i++){
                            console.log(v[i])
                            if(Number(v[i])>=2 && Number(v[i])<=3){
                                return (
                                    <div onClick={()=>this.ProductClick(item.value.name)} key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                                            
                                            <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                           <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                                  
                                      
                                               </div> 
                    
                    
                    
                                           <div className="col-5">   
                                         <h5><span className='ProductName'>{item.value.name}({v[i]}kg)</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                           <p style={{marginTop:'3px',lineHeight:'20px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                            </div>
                    
                    
                                        
                                             <div className='col-3'>
                                            
                                            <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                            <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                             <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                            
                    
                                              </div>
                                 
                                          
                                             </div>
                                       )
                            }
                        }
                     }
                      
                    })
                           
                  
                  }
            </div>)
            case 4: return(<div>
                   {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}      
                {this.state.allProducts.map( (item)=>{
                     
                     var v=item.value.size.split(" ")
                     console.log(v)
                     if(v.length>1){
                        for(var i=0;i<v.length;i++){
                            console.log(v[i])
                            if(Number(v[i])>=3 && Number(v[i])<=4){
                                return (
                                    <div onClick={()=>this.ProductClick(item.value.name)} key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                                            
                                            <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                           <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                                  
                                      
                                               </div> 
                    
                    
                    
                                           <div className="col-5">   
                                         <h5><span className='ProductName'>{item.value.name} ({v[i]}kg)</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                           <p style={{marginTop:'3px',lineHeight:'20px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                            </div>
                    
                    
                                        
                                             <div className='col-3'>
                                            
                                            <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                            <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                             <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                            
                    
                                              </div>
                                 
                                          
                                             </div>
                                       )
                            }
                        }
                     }
                      
                    })
                           
                  
                  }
            </div>)
            case 5: return(<div>
                 {this.state.productclick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}    

                {this.state.allProducts.map( (item)=>{
                     
                     var v=item.value.size.split(" ")
                     console.log(v)
                     if(v.length>1){
                        for(var i=0;i<v.length;i++){
                            console.log(v[i])
                            if(Number(v[i])>=4 && Number(v[i])<=5){
                                return (
                                    <div onClick={()=>this.ProductClick(item.value.name)}  key={item.name} className="card col-12" style={{width:'60rem',marginTop:'5px',padding:'10px',border:'none',backgroundColor:"white",display:'flex',flexDirection:'row',justifyContent:'space-around',cursor:'pointer'}}>
                                            
                                            <div className="col-4" style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
                                           <img src={item.value.image1} style={{width:'200px',height:'auto',cursor:'pointer'}} class="card-img-top" alt="not load"/> 
                                  
                                      
                                               </div> 
                    
                    
                    
                                           <div className="col-5">   
                                         <h5><span className='ProductName'>{item.value.name}({v[i]}kg)</span><span style={{backgroundColor:'green',width:'20px',color:'white',padding:'3px',borderRadius:'5px',marginLeft:'3px',cursor:'pointer'}}>{<i class="fa fa-star" style={{color:'yellow'}} aria-hidden="true">({item.value.rating})</i>}</span></h5>
                                           <p style={{marginTop:'3px',lineHeight:'20px'}}><div dangerouslySetInnerHTML={{__html:item.value.shortdescription}} /></p>     
                                            </div>
                    
                    
                                        
                                             <div className='col-3'>
                                            
                                            <p><i class="fa fa-inr" aria-hidden="true"></i><b>{(item.value.price)-(item.value.price)*(item.value.offer)/100}</b></p>
                                            <p><s><i class="fa fa-inr" aria-hidden="true"></i>{item.value.price}</s> <span style={{color:'green',fontWeight:'bold'}}> {item.value.offer}<i class="fa fa-percent" aria-hidden="true"></i> off</span></p>
                                             <p><b>Save</b> <i class="fa fa-inr" aria-hidden="true"> {(item.value.price)*(item.value.offer)/100}</i>  </p>
                                            
                    
                                              </div>
                                 
                                          
                                             </div>
                                       )
                            }
                        }
                     }
                      
                    })
                           
                  
                  }
            </div>)
       
        }
    }
}
