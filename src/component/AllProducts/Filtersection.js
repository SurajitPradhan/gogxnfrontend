import { Checkbox } from '@material-ui/core'
import axios from 'axios'
import React, { Component } from 'react'

export default class Filtersection extends Component {
   constructor(){
       super()
       this.state={
            producttype:false,
            productName:'',
            allproduct:[]
       }
   }


   componentDidMount(){
       axios.get('http://65.0.249.137:8000/api/product/').then(res=>{
                  
                  let product=[]
                  for(var i=0;i<res.data.length;i++){
                      product.push({name:i,value:res.data[i]})
                  }
                  this.setState({
                     allproduct:product
                  })
       })
      
   }





   ChangeHandler=(v)=>{

    this.setState({
        productName:v
    })
    console.log(v)
    this.props.handlefilter(this.state.productName)
   }



    render() {
        return (
            <React.Fragment>
                <div>
                 

                <div class="product_tabs ">
              <div class="product_tab ">
                <input hidden type="checkbox" id="ptype"/>
                <label class="product_tab-label" for="ptype">Product Type</label>
                <div class="product_tab-content overflow-auto" >
                    <p className="product_contents">
                     <ul>
                         <li><input type='checkbox'  /> Health Supplements</li>
                         <li><input type='checkbox' /> Mass Gainer</li>
                         <li><input type='checkbox' /> Pre Work Out</li>
                         <li><input type='checkbox' /> Post Work Out</li>
                     </ul>
                </p>
                </div>
              </div>
           
              </div>

              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="price"/>
                <label class="product_tab-label" for="price">Price</label>
                <div class="product_tab-content overflow-auto" >
                    <p className="product_contents">
                     <ul>
                         <li><input type='checkbox' /> Low To High</li>
                         <li><input type='checkbox' /> High To Low</li>
                        
                     </ul>
                </p>
                </div>
              </div>
           
              </div>


              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="size"/>
                <label class="product_tab-label" for="size">Size</label>
                <div class="product_tab-content overflow-auto" >
                    <p className="product_contents">
                     <ul>
                         <li><input type='checkbox' /> 0-1 lbs</li>
                         <li><input type='checkbox' /> 1-2 lbs</li>
                         <li><input type='checkbox' /> 2-3 lbs</li>
                         <li><input type='checkbox' /> 3-4 lbs</li>
                         <li><input type='checkbox' /> 4-5 lbs</li>
                     </ul>
                </p>
                </div>
              </div>
           
              </div>

              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="discount"/>
                <label class="product_tab-label" for="discount">Discount Wise</label>
                <div class="product_tab-content overflow-auto" >
                    <p className="product_contents">
                     <ul>
                         <li><input type='checkbox' onClick={()=>this.ChangeHandler(10)}  /> More than 40%</li>
                         <li><input type='checkbox' /> More than 30%</li>
                         <li><input type='checkbox' /> More than 20%</li>
                         <li><input type='checkbox' /> More than 10%</li>
                     </ul>
                </p>
                </div>
              </div>
           
              </div>

        



              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="flavour"/>
                <label class="product_tab-label" for="flavour">Flavour</label>
                <div class="product_tab-content overflow-auto" >
                    <p className="product_contents">
                     <ul>
                         <li><input type='checkbox' /> Chocolate</li>
                         <li><input type='checkbox' /> Mango</li>
                         <li><input type='checkbox' /> Strawberry</li>
                         <li><input type='checkbox' />Banana</li>
                     </ul>
                </p>
                </div>
              </div>
           
              </div>



                </div>
            </React.Fragment>
        )
    }
}
