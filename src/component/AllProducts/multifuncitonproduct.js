import React, { Component } from 'react'
import Product from './Product'
import './style.css'
export default class FiltrationProduct extends Component {
    render() {
        return (
            <React.Fragment>
                  <div className='outerdiv'>  
                      <div class='inner'>
                      <table>
                          <ul>
                              <li> <h4>Product Types</h4>
                                  <ul>
                                      <li>Health supplements</li>
                                      <li>Mass gainer</li>
                                      <li>Weight loss</li>
                                  </ul>
                              </li>

                              <li>
                                  <h4>Price </h4>
                                  <ul>
                                      <li>Low to High</li>
                                      <li>High to low</li>
                                  </ul>
                              </li>

                              <li>
                                  <h4>Flavour</h4>
                                  <ul>
                                      <li>Chocolate</li>
                                      <li>Mango</li>
                                  </ul>
                              </li>

                              <li>
                                  <h4>Discount</h4>
                                  <ul>
                                  <li>More than 40%</li>
                                  <li>More than 30%</li>
                                  </ul>
                              </li>
                          </ul>
                      </table>
                          
                     </div>


                      <div class='inner2'><Product data={'hello'} /></div>
                  </div>
            </React.Fragment>
        )
    }
}
