import React, { Component } from 'react'

export default class Product extends Component {
    render() {
        return (
            <React.Fragment>
                This is the product page section.
                 {this.props.data}
            </React.Fragment>
        )
    }
}
