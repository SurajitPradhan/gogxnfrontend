import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import BlogCard from '.';

import './style.css'
import axios from 'axios';
// import image from '../Images/healthxp1.jpg';
// import Card from './Card';
const options = {
  margin: -10,
 margin: 15,  
  // padding: 50,
  responsiveClass: true,
  nav: true,
  autoplay: false,
 loop:true,
  smartSpeed: 1000,
  responsive: {
      0: {
          items: 1,
      },
      400: {
          items: 1,
      },
      600: {
          items: 1,
      },
      700: {
          items: 2,
      },
      1000: {
          items: 4,
      }
  },
};

class Homepageblog extends React.Component {

  constructor(props) {
    super(props)
  
    this.state = {
       image:[],
       title:[],
       comment:[],
       text:[]
    }
  }
  

  componentDidMount=()=>{
    axios.get('http://65.0.249.137:8000/api/blogsection/')
    .then((res)=>{
      for (var i = 0; i < res.data.length; i++)
     
      this.setState({
        title: [...this.state.title, res.data[i].title],
        text: [...this.state.text, res.data[i].text],
        comment: [...this.state.comment, res.data[i].comment],
        image: [...this.state.image, res.data[i].image],
        // data:[res.data]
      })

      console.log(res)
    })
  }
  render() {
   
    return (
<div class="carousel2  px-2"><center>
  <h1 class="blog" mt-5 py-5>  </h1></center>
  
<OwlCarousel className="d-lg-block d-none  blog-items owl-carousel px-5" {...options}>
  {/* <div className="blogs px-5 "> */}
                       <div class="item px-2"><BlogCard imgsrc={this.state.image[0]}
                       heading={this.state.title[0]}
                       text={this.state.text[0]}
                       btn="Read more"/></div>
  
                      <div class="item px-2"><BlogCard imgsrc={this.state.image[1]}
                      heading={this.state.title[1]}
                      text={this.state.text[1]}
                      btn="Read more"/></div>
                      
                       
                      <div class="item px-2"><BlogCard imgsrc={this.state.image[2]}
                      heading={this.state.title[2]}
                      text={this.state.text[2]}
                      btn="Read more"/></div> 

                     <div class="item px-2"><BlogCard imgsrc={this.state.image[3]}
                      heading={this.state.title[3]}
                      text={this.state.text[3]}
                      btn="Read more"/></div>
                   
                
                     <div class="item px-2"><BlogCard imgsrc={this.state.image[4]}
                      heading={this.state.title[4]}
                      text={this.state.text[4]}
                      btn="Read more"/></div> 
                     
                     
                      
                      
                      {/* <div class="item"><BlogCard imgsrc={image}/></div>
                      <div class="item"><BlogCard imgsrc={image}/></div>
                      <div class="item"><BlogCard imgsrc={image}/></div>
                      <div class="item"><BlogCard imgsrc={image}/></div> */}
{/* </div> */}
                      
                  </OwlCarousel> 
</div>
)
};
}

export default Homepageblog;


