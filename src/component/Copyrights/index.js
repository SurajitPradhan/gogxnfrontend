import React, { Component } from 'react'
import './style.css'
export class Copyrights extends Component {
    render() {
        return (
            <div className="copyright_section">
           
              <div className="copyright_details mt-2  mb-1"><center>
                    <p className="copyright_info ">Copyright 2021 gogxn.com. All Rights Reserved</p>
                    </center>
                    </div>
                    <div class="horizontal-rule"></div>

                
                <div className="copyright_details mt-2 "><center>
                    <p className="disclaimer"><strong>Disclaimer:</strong> All GXN products are manufactured at FSSAI approved manufacturing 
                    facilities and are not intended to diagnose, treat, cure, or prevent any disease.
                     Please read product packaging carefully prior to purchase and use .</p></center>
                </div>
              
            </div>
        )
    }
}

export default Copyrights
