import React, { Component } from 'react'
import axios from 'axios'
import server from '../server/server';
export class DetailPageDes extends Component {

  constructor(){
    super()
    this.state={
      allProducts:[]
    }
  }


  componentDidMount=()=>{

    
    // axios.get(`${server2}api/product/`).then((res)=>{

    axios.get(`${server}api/product/`).then((res)=>{

      let product=[]
      for(var i=0;i<res.data.length;i++){
        if(this.props.data==res.data[i].name){
          product.push({name:i,value:res.data[i]})
        }
      }
    
      this.setState({
        allProducts:product
      })

    })
  }

   
  
  render() {
    return (
      <>
    {
          this.state.allProducts.map((item)=>{

            return(
            <div className="product_overview ">
          
    
            <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck1"/>
                <label class="product_tab-label" for="chck1"><b>Product Overview</b></label>
                <div class="product_tab-content overflow-auto" >
                    <p className="product_contents">
                      <div style={{fontFamily:"Roboto,Arial,sans-serif", lineHeight:"25px"}} dangerouslySetInnerHTML={{__html:item.value.description}} />
                </p>
                </div>
              </div>
           
              </div>
        
          
        
        
              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck2"/>
                <label class="product_tab-label" for="chck2"><b>Ingredients</b></label>
                <div class="product_tab-content overflow-auto">
                    <p className="product_contents" style={{lineHeight:"25px"}}>
                Yes, we are a prominent supplier and manufacturer of all kinds of Health’s nutrition including mass gainer, whey protein, fat burner and workout essentials.
                </p>
                </div>
              </div>
           
              </div>
        
           
              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck3"/>
                <label class="product_tab-label" for="chck3"><b>Nutritional Information</b></label>
                <div class="product_tab-content overflow-auto" style={{textAlign:'center', fontFamily:"Roboto,Arial,sans-serif"}}>
                   
                 <img src={item.value.Nutritional_information} height='auto' width='500px' alt='image'/>
              
                </div>
              </div>
           
              </div>
        
              <div class="product_tabs">
              <div class="product_tab">
                <input hidden type="checkbox" id="chck4"/>
                <label class="product_tab-label" for="chck4"><b>Why Choose ?</b></label>
                <div class="product_tab-content overflow-auto">
                <p className="contents">

                <ul class="list-group">
           <li class="list-group-item" >Free Fast Delivery</li>
        <li class="list-group-item">Good Quality Product</li>
           <li class="list-group-item">Find the Original Product </li>
          <li class="list-group-item">Hashle Free Return</li>
           <li class="list-group-item">This is a Complete Natural Product</li>
            </ul>
                 </p>
                </div>
              </div>
           </div>
           
        
            </div>)
        
          })
        }

      </>
    )
  }
}

export default DetailPageDes
