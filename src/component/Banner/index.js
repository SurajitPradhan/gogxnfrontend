import axios from 'axios'
import React, { Component } from 'react'
import { Redirect } from 'react-router'
import server from '../server/server'
import './style.css'
class Banner extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name:[],
             image:[],
             activeset:'',
             index:0,
             button:false,
             autoindex:0,
             productClick:false,
             productname:''
        }
        this.Prev=this.Prev.bind(this)
        this.Next=this.Next.bind(this)
        this.AutomatedChange=this.AutomatedChange.bind(this)
        this.ProductClick=this.ProductClick.bind(this)
    }
    // http://127.0.0.1:8000/sliderimage/
    componentDidMount(){
        axios.get(`${server}api/banner/`)
        // axios.get('http://65.0.249.137:8000/api/banner/')
        .then(response=>{
            for (var i = 0; i < response.data.length; i++)
            this.setState({
                name:[...this.state.name, response.data[i].name],
                image: [...this.state.image, response.data[i].image],
                activeset:this.setState.activeset

            })
            
        })


        
    

    }

    Prev(){
        var index=this.state.index;
      
        if(index==0){
            this.setState({
                index:(this.state.image.length-1),
                button:true

            })
        }else{
            this.setState({
                index:index-1,
                button:true
            })
        }
      

    }


    Next(){

        var index=this.state.index;

        
       if(index==(this.state.image.length-1)){
        this.setState({
            index:0,
            button:true
        })

       }else{
        this.setState({
            index:index+1,
            button:true
        })
       }


    }


    AutomatedChange(){
        var autoindex=this.state.autoindex
       
            setTimeout(()=>{
                
                if(autoindex>this.state.image.length){
                    this.setState({
                        autoindex:0
                    })
                }else{
                    this.setState({
                        autoindex:autoindex+1
                    })
                }
    
                
            },4000)
        
    }
 



    ProductClick=(v)=>{

        this.setState({
            productClick:true,
            productname:v
        })

    }

    render() {

        
    
           
         

        
        return (
               <React.Fragment>

                   {this.state.productClick?<Redirect to={{pathname:`/productpage/${this.state.productname}`,state:{id:this.state.productname}}} />:''}

                   <div id="myCarousel" class="carousel slide" data-ride="carousel">
                   <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li></ol>
   
  <div class="carousel-inner">
 
            <div  onClick={()=>this.ProductClick(this.state.name[this.state.index])} class="carousel-item active">
                <img src={this.state.image[this.state.index]} style={{cursor:'pointer'}} class="d-block banner_images  w-100" alt="..."/>
            </div>

             
   
   
  </div>



  <button onClick={this.Prev} style={{backgroundColor:'transparent',border:'none'}} class="carousel-control-prev" to="#myCarousel"  type="button"   >
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
   
  </button>


  <button onClick={this.Next} style={{backgroundColor:'transparent',border:'none'}} class="carousel-control-next" type="button" >
  
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    
  </button>
 
</div>


               </React.Fragment>
        )
    }
}

export default Banner;

