import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './style.css'
import sitelogo from '../../images/sitelogo.png'
export class Navbar extends Component {
    render() {
        return (<>

<nav >
      {/* <div class="menu-icon"> */}
      <input type="checkbox" id="click"/>
      <label for="click" class="menu-btn px-3">
        <i class="fa fa-bars"></i>
        
      </label>
{/* <span class="fa fa-bars"></span></div> */}
<div class="logo ">
<Link to='/home'><img src={sitelogo} width="100px"/></Link></div>
 
<div class="nav-items">
<li><Link class="active" to="/allproducts">SHOP</Link></li>
<li><Link  to="/deals">DEALS</Link></li>
<li><Link  to="">NEW</Link></li>
<li><Link  to="/authenticity">Authenticity</Link></li>
<li><Link  to='/blogsection'>BLOG</Link></li>
<li><Link  to="/overview">OVERVIEW</Link></li>
<li><Link  to="/wishlist">WISHLIST</Link></li> 
</div>


<div class="search-icon">

 <Link to='/signup'> 
<b><i class="fa fa-user-o  pr-4" aria-hidden="true" style={{color:"black"}}></i></b></Link>
<Link to='/cartvalue'><i class="fa fa-shopping-bag " aria-hidden="true" style={{color:"black"}}></i></Link>


</div>


</nav>
<div className="searchs py-1" style={{background:"grey"}}>
<div className="search_box px-3">
                <div className="input-group ">
                    <input style={{borderRadius:"5px",padding:'3px'}} type="text" className="form-control" placeholder="Search here..." name="search"/>
                   
                </div>
                
            </div></div>
      </>  )
    }
}

export default Navbar
