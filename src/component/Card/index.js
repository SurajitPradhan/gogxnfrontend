import React, { Component } from 'react'
import './style.css'

const Card=(props)=> {
   
        return (
            <div class="card's" style={{cursor:'pointer'}}>
              <a class="icon pl-3 "> <i class="fa fa-heart-o fa-center" aria-hidden="true"  ></i></a> 
             <div className="div " >
{/* px-5 */}
               <center>
  <img class="card-pic-top pr_img" src={props.image} alt="Responsive image"  /></center></div>
  {/* <div class="card-body"> */}
  
   <p class=" product_names mt-3 pl-3"><b>{props.name} </b></p>
    <div className="star_icons ">
    <div class="row"> 

    <i class="fa fa-star" aria-hidden="true"style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>

</div></div>
<p class="card-price px-3"><b>Rs.{props.price}</b></p>

  </div>
 
 
// </div>

 

        )
    }


export default Card
