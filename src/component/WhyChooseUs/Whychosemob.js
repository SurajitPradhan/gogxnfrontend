import React, { Component } from 'react'
import server from '../server/server';
import axios from 'axios'
export class Whychosemob extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            data:[]
        }
    }
  
    componentDidMount(){
        axios.get(`${server}api/whychooseus/`)
        .then((res)=>{
            let datavalue=[]
           for (var i = 0; i < res.data.length; i++){
                 datavalue.push({name:i,value:res.data[i]})
           }

           this.setState({
                 data:datavalue
           })
         
        })
    }
    render() {
        
        return (
            <div className="mobileview px-4 ">
                <center>
                <div className="row">
                {
                          this.state.data.map((item)=>{
                              if(item.value.id){
                                  return (<>
                                                                        
                                       <div className="col-2  d-lg-none ">
                                            <center><br/>
                                  <img src={item.value.image}class="img-fluid" alt="Responsive image" width="30px"/>
                                      <strong>
                                  <p className="image_text_para">{item.value.title}</p></strong></center>               
                                 </div>
                                          </>
                                  )
                              }else{
                                  return (<h1>there is problem</h1>)
                              }
                          })
                        }
                               

                    
              
                                </div>
                </center>
            </div>
        )
    }
}

export default Whychosemob
