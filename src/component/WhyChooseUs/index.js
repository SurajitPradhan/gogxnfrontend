import axios from 'axios'
import React, { Component } from 'react'
import './style.css'
import server from '../server/server';
import Navbar from '../Navbar';

 class WhyChooseus extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
             data:[]
         }
     }
   
     componentDidMount(){
         axios.get(`${server}api/whychooseus/`)
         .then((res)=>{
             let datavalue=[]
            for (var i = 0; i < res.data.length; i++){
                  datavalue.push({name:i,value:res.data[i]})
            }

            this.setState({
                  data:datavalue
            })
        //   console.log(datavalue)
         })
     }
    render() {
        return (
            <div>
            
                <h3 className="why_choose_us   px-1"><b>Why Choose us ?</b></h3>
                <h6 className="why_choose px-3 mt-3">You can count on us & we mean it!</h6>
                <p className="why_chose_gxn px-4 py-1">Our products are formulated with International standards and are highly result oriented. Made with premium ingredients and are highly rich in protein.</p>
                <div className="whychooseus_images">
                <div class="Whychoseus_container d-none d-lg-block px-5">
                     <div class="row">
                       
                        {
                          this.state.data.map((item)=>{
                              if(item.value.id){
                                  return (<>
                                    <div class=" col-sm mt-2"><center><br/>
                                    <img src={item.value.image} class="img-fluid" alt="Responsive image"/>
                                    <strong>
                                    <p className="image_para">{item.value.title}</p></strong></center>
                                       </div>
                                    
                                          </>
                                  )
                              }else{
                                  return (<h1>there is problem</h1>)
                              }
                          })
                        }
                               


                           
                 </div>
               </div>
                </div>
                
                
                            </div>
        )
    }
}

export default WhyChooseus;
