




import React from 'react'
import './style.css'
const ProductCarousel=(props)=> {
    return (
        <div className="acces_container " style={{background:" rgb(243, 241, 241) "}}>
      <div className="product_outline"><center>
        <div className=" row">
            {/* <div className="col-md-3 col-sm-6"> */}
                <div className="accessories-grid">
                    <div className="accessories-image">
                    
                        <a href="#" className="access-image" style={{background:" rgb(243, 241, 241) "}}>

                            <img className="acc_pic1" 
                            src={props.image1}
                            alt ="responsive img"/>
                            <img className="acc_pic2" 
                            src={props.image2}
                             alt ="responsive img"/>
                        </a>
                     {/* <li className="badge"><i class="fa fa-certificate fa-4x" aria-hidden="true"></i>
</li>    */}

                        <ul className="social_links"><center>
                            <li><a href="#"><i class="fa fa-heart-o " aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></li>
                            </center>
                        </ul>
                        <center>
                          <p className="protein_naming px-3">{props.name}</p> 
                          <div className="rating">
            <i class="fa fa-star" aria-hidden="true" style={{color:"goldenrod", fontSize:"12px"}}></i>
            <i class="fa fa-star" aria-hidden="true"  style={{color:"goldenrod", fontSize:"12px"}}></i>
            <i class="fa fa-star" aria-hidden="true"  style={{color:"goldenrod", fontSize:"12px"}}></i>
            <i class="fa fa-star" aria-hidden="true"  style={{color:"goldenrod", fontSize:"12px"}}></i>
            <i class="fa fa-star-half-o" aria-hidden="true"  style={{color:"goldenrod", fontSize:"12px"}}></i>
            (8 customer review)
            </div>
            </center>
                          
                         <div className="ofer ">
                         <p ><del>Rs.{props.text}</del> - <strong style={{color:"red"}}>Rs.{(((Number(props.text)-Number(props.cashback))-Number(props.text)*Number(props.discount)/100))}</strong>
                     </p> <p><b>Save:</b><strong style={{color:"red"}}> Rs.{(Number(props.text)*Number(props.discount)/100)+ Number(props.cashback)}  ({props.discount}%) </strong> </p>
                     
                      </div>
                      
                    </div>
                    
                </div>
            </div>
  
            </center>
      </div>
           
        </div>
     
    )
}

export default ProductCarousel
