import React, { Component } from 'react'
import './style.css'
import { Link } from 'react-router-dom';
export class Mobo_Footer extends Component {
    render() {
        return (
            
<>
<div class="accordion d-md-none d-block">
       
        <span class="target-fix" id="accordion"></span>
         
       
        <div>
           
            <span class="target-fix" id="accordion1"></span>
            
 
            <a href="#accordion1" id="open-accordion1" title="open"> <h5> <strong style={{color:"white"}}>Support</strong></h5></a>
            
           
            <a href="#accordion" id="close-accordion1" title="close"><h5> <strong style={{color:"white"}}>Support</strong></h5></a> 
            
          
            <div class="accordion-content " style={{color:"black"}}>
              <div className="row">        
            
                <div className="footer_col col-sm-6 px-4"> <Link to='/frequentlyaskquestions'> <li ><b>FAQs</b></li></Link>
       <Link to="/carrer"><li><b>Careers</b></li></Link>
           <Link to='/terms&condition'> <li><b>Terms & Conditions</b></li></Link> 
           <Link to='/privacy&policy'><li><b>Privacy & Policy</b></li></Link>   
             <Link to='/refund&policy'><li><b>Refunds & Cancellations</b></li></Link>  </div>
             
             
             
             
             
                </div>

            </div>
        </div>
        <div>
            <span class="target-fix" id="accordion2" style={{background:"blue"}}></span>
            <a href="#accordion2" id="open-accordion2" title="open"><h5 > <strong style={{color:"white", paddingTop:"10px"}}>Get Social</strong> </h5></a>
            <a href="#accordion" id="close-accordion2" title="close"><h5><strong style={{color:"white"}}>Get Social</strong></h5></a>
            <div class="accordion-content">
            <div className=" segment-three">


<div style={{display:'flex',flexDirection:'row', paddingLeft:"55px", paddingBottom:"23px", paddingTop:"20px"}} >

<a style={{display:'flex',alignItems:'center',justifyContent:'center'}}  className="icon " href="https://www.facebook.com/gogxn/"><i style={{color:'white'}} class="fa fa-facebook fa-1.5x"></i></a>&nbsp;
<a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://twitter.com/go_gxn"><i style={{color:'white'}} className="fa fa-twitter fa-1.5x"></i></a>&nbsp;
<a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://www.flickr.com/people/158004145@N04/"><i style={{color:'white'}} className="fa fa-flickr fa-1.5x"></i></a>&nbsp;
<a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://in.pinterest.com/go_gxn/"><i style={{color:'white'}} className="fa fa-pinterest fa-1.5x"></i></a>&nbsp;
<a style={{display:'flex',alignItems:'center',justifyContent:'center'}} className="icon" href="https://www.youtube.com/channel/UCa4cEu-G1AejDb27q66L5ag"><i style={{color:'white'}} className="fa fa-youtube fa-1.5x"></i></a>


</div>


</div>

            </div>
        </div>

        <div>
            <span class="target-fix" id="accordion3" style={{background:"blue"}}></span>
            <a href="#accordion3" id="open-accordion3" title="open"><h5> <strong style={{color:"white"}}>Subscribe Us</strong> </h5></a>
            <a href="#accordion" id="close-accordion3" title="close"><h5><strong style={{color:"white"}}>Subscribe Us</strong></h5></a>
            <div class="accordion-content">
            <div className=" segment-three px-3 py-3">
            <form action="">
            <div class="inputWithIcon  col-md-12 mt-2 ">
                 <input type="text" class="form-control" placeholder="Your name" />
                   <h3>   <i class="fa fa-user " aria-hidden="true"></i></h3> 
                 </div>
               

                 <div class="row px-3">
             <div class="inputWithIcon  col-md-6 mt-4 ">
                 
             <input type="email" class="form-control" placeholder="Email"/>
             <h3><i class="fa fa-envelope" aria-hidden="true"></i></h3>  
             </div>
                       </div>   
                         {/* <input type="text" class="form-control " id="inputnamel4" placeholder="Name"/>
                            <input type="email" class="form-control mt-2" id="inputEmail4" placeholder="Email"/>
                            */}
                            <button type="button" class="btn btn-primary mt-3 ml-4">Subscribe</button>
                            </form> 


</div>

            </div>
        </div>



</div>



</>
      )
    }
}

export default Mobo_Footer
