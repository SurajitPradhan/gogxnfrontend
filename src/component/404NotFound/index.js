import React, { Component } from 'react'
import './style.css'
import computeriamge from '../../images/computer.png'
import { Link } from 'react-router-dom'

export default class NotFound extends Component {
    render() {
        return (
            <React.Fragment>

              <div className='fourouter'>
                 
                <h2 className='text-center' style={{fontWeight:'800',fontSize:'40px'}}>404 Page Not Found</h2>
              </div>
              <div className='text-center p-5' >
                 <img src={computeriamge} />
                 <h5 style={{fontWeight:'800',color:'black'}}>Unfortunately the page which you are looking for is removed or deleted</h5>
                <Link to='/home'><button className='btn btn-primary mt-4' >Goto Homepage</button></Link>   
              </div>
            </React.Fragment>
       
        )
    }
}
