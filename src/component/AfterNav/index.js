
import React ,{Component, useCallback} from 'react'
import '../AfterNav/style.css'
import logo4 from '../../images/logo4.png'
import axios from 'axios';
import {Link} from 'react-router-dom'

import { Modal,Button, OverlayTrigger, NavLink } from 'react-bootstrap';
import { Popover } from 'react-bootstrap';
class AfterNav extends Component{
    constructor(){
        super();
        this.state={
            shop:false,
            deals:false,
            protien:'',
            category:'',
            count:0,
            allproducts:false
        }
        this.Shop=this.Shop.bind(this)
        this.Deals=this.Deals.bind(this)
        this.ShopClick=this.ShopClick.bind(this)
    }


  Shop(){
      this.setState({
          shop:!this.state.shop
      })
  }

  Deals(){
        this.setState({
            deals:!this.state.deals
        })
  }
  // http://127.0.0.1:8000/protiensection/

  componentDidMount(){
    axios.get('http://65.0.249.137:8000/api/product/').
    then(res=>{
      this.setState({protien:res.data})
    })

    axios.get('http://65.0.249.137:8000/api/categorysection//').
    then(res=>{
      this.setState({
         category:res.data
      })
    })
  }

  ShopClick(){
    this.setState({
      allproducts:true
    })
  }



    render(){

        return(
          <div>
          
            <div className='apps'>
                
                    <div className='afternav row '>
                  
                    
                    <ul className='afternavul container mr-5 col-8'>    
                <li>   
                    <div class="dropdown1">
                <a href='/allproducts' style={{color:'black'}}><button class="dropbtn" onClick={this.ShopClick} style={{color:'black'}}>SHOP
                  </button></a>   

                 <div class="dropdown-content1 border rounded">
               
                 <div class="row">
                {this.state.category?
                 <div className='shopcategory' style={{display:'flex',flexDirection:'row'}}>
                  {this.state.category.map((item)=>
                  <ul key={item} style={{color:'black'}}>
                  <h5 className='col-6' style={{textDecoration:'underline',color:'black'}}>{item.name}</h5>
                  <li>Why protien</li>
                  <li>Gold gxn protien</li>
                  <li>Weight gainer</li>
                  <li>Mass gainer</li>
                  <li>best in market</li>
                </ul>)}
                 </div>
               :''
              
              }
              </div>
                
                        
              
              </div> 
                </div>
                
                
            </li>

                    <a href='/deals'> <li className='bground'>
                    <a className='ba_me' style={{color:"black"}}>DEAL</a>
                      </li> </a>

                    <li className='bground'>
                    <a style={{color:'black'}}>NEW</a>
                    </li>

                 <Link to="/authenticity"> <li className='bground'>
                     <a style={{color:"black"}}>AUTHENTICITY</a>
                      </li></Link>   

                 <Link to='/blogsection'>
                 <li className='bground' >
                     <a style={{color:"black"}}>BLOGS</a>
                      </li>
                 </Link> 
                 

                       <Link to="/overview">
                    <li className='bground'>
                     <a style={{color:"black"}}>OVERVIEW</a>
                      </li></Link>
                    
                   </ul>    
               
               
                    </div>


                     
                  


                  {/* mobile compatible */}
            
                

            </div>
 </div>

        )
    }
}

export default AfterNav;