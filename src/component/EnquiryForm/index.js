import { responsiveFontSizes } from '@material-ui/core';
import React, { Component } from 'react'
import support from '../../images/support.svg'
import { Link, Redirect } from 'react-router-dom';
import './style.css'
import axios from 'axios';
import server from '../server/server';

export class EnquiryForm extends Component {

  constructor(){
    super()
    this.state={
      name:'',
      email:'',
      phone:'',
      subject:'',
      notes:'',
      error:'',
      success:'',
      homepage:false,
    }

    this.InputChange=this.InputChange.bind(this)
    this.ResetForm=this.ResetForm.bind(this)
    this.ButtonSubmit=this.ButtonSubmit.bind(this)
    
  }


  InputChange(event){
    this.setState({
      [event.target.name]:event.target.value
    })
}



ButtonSubmit(){
 alert('button click')
 if(!this.state['name']){
     this.setState({
         error:'Name Can not be null',
         success:'',
        
     })
     
   }
   else if(!this.state['email']){
       this.setState({
           error:'Email Can not be null',
           success:'',
           
       })
   }
   else if(!this.state['phone']){
       this.setState({
           error:"Phone Should not be null",
           success:'',
         
       })
   }else if(this.state.phone.length>10 | this.state.phone.length<10){
       this.setState({
           error:'Number Should be 10 digit',
           success:'',
           
       })
   }
   else if(!this.state['subject']){
        this.setState({
            error:'Password fill not be null',
            success:'',
           
        })
   }else if(!this.state['notes']){
       this.setState({
           error:'Confirm password not be null',
           success:'',
           
       })
   } else{

     axios.post(`${server}api/carreer/`,{
         name:this.state.name,
         email:this.state.email,
         phone:this.state.phone,
         subject:this.state.subject,
         notes:this.state.notes
     })
     .then(res=>{

       
     this.setState({
       error:'',
       success:'Your form is submited successfully please wait for redirect..',
     })
       
     this.ResetForm()

     }).catch(er=>{
        
        
     })

   
}
}


ResetForm(){
setTimeout(() => {
  
  this.setState({
    name:'',
    email:'',
    phone:'',
    password:'',
    confirmpassword:"",
    homepage:true,
   })
   window.location.reload()
}, 2000);


}




    render() {
        return (
            <div className="contacts_form ">
                <h2 className="enquire pt-4 pb-5" style={{textAlign:"center"}}><b>Apply For Opportunity</b></h2>
             <div className="contact_form row  ">

            


             <div className="form_left col-md-4">
<img src={support} />

                </div>
                <div className="form_right col-md-8 ">

                <div className='text-center col-12'>
        {this.state.error?<div class="alert mx-auto col-12 alert-danger" role="alert">
   {this.state.error}
</div>:''}

{this.state.success?<div class="alert col-12 mx-auto alert-success" role="alert">
  {this.state.success}
</div>:''}
{this.state.homepage?<Redirect to='/home' />:''}
</div>


                <form>
{/* Name section */}
                <div class="inputWithIcon  col-md-12 mt-2 ">
                 <input type="text" name='name' value={this.state.name} onChange={this.InputChange} class="form-control" placeholder="Your name" />
                   <h3>   <i class="fa fa-user " aria-hidden="true"></i></h3> 
                 </div>

{/* email section */}
             <div class="row px-3">
             <div class="inputWithIcon  col-md-6 mt-4 ">
                 
             <input type="email" name='email' value={this.state.email} onChange={this.InputChange} class="form-control" placeholder="Email"/>
             <h3><i class="fa fa-envelope" aria-hidden="true"></i></h3>  
             </div>
{/* PhoneNumber section */}
            <div class="inputWithIcon  col-md-6 mt-4 ">
              {/* inputIconBg */}
            <input type="number" name='phone' value={this.state.phone} onChange={this.InputChange} class="form-control" placeholder="Phone No." />
            <h3>  <i class="fa fa-phone-square" aria-hidden="true"></i></h3> 
                </div>
                </div>

{/* subject section */}
                <div class="inputWithIcon  col-md-12 mt-4 ">
                 <input type="text" name='subject' value={this.state.subject} onChange={this.InputChange} class="form-control" placeholder="Subject" />
                   <h3> <i class="fa fa-file-text" aria-hidden="true"></i></h3> 
                 </div>

{/* Message section */}
                 <div class="inputWithIcon   col-md-12 mt-4  ">
                 {/* <input type="text" class="form-control"  rows="5" /> */}
                 <textarea class="form-control" value={this.state.notes} onChange={this.InputChange} name='notes' id="exampleFormControlTextarea1" placeholder="Notes" rows="5"></textarea>
                 </div>

             </form>
   <button type="button" class="btn btn-outline-info mt-4 mb-5 ml-3" onClick={this.ButtonSubmit} ><b>Apply</b></button>
            
                </div>
             
             </div>
            </div>
        )
    }
}

export default EnquiryForm;
