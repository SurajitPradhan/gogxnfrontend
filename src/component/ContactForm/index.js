import { responsiveFontSizes } from '@material-ui/core';
import React, { Component } from 'react'
import './style.css'
export class ContactForm extends Component {
    render() {
        return (
            <div className="contacts_form ">
                <h1 className="contact_with_us pt-2 pb-5"><b>Contact Us</b></h1>
             <div className="contact_form row  ">
             <div className="form_left col-md-4">
<img src="https://www.emailjs.com/assets/support.svg" />
                </div>
                <div className="form_right col-md-8 ">
                <form>
{/* Name section */}
                <div class="inputWithIcon  col-md-12 mt-2 ">
                 <input type="text" class="form-control" placeholder="Your name" />
                   <h3>   <i class="fa fa-user " aria-hidden="true"></i></h3> 
                 </div>

{/* email section */}
             <div class="row px-3">
             <div class="inputWithIcon  col-md-6 mt-4 ">
                 
             <input type="email" class="form-control" placeholder="Email"/>
             <h3><i class="fa fa-envelope" aria-hidden="true"></i></h3>  
             </div>
{/* PhoneNumber section */}
            <div class="inputWithIcon  col-md-6 mt-4 ">
              {/* inputIconBg */}
            <input type="number" class="form-control" placeholder="Phone No." />
            <h3>  <i class="fa fa-phone-square" aria-hidden="true"></i></h3> 
                </div>
                </div>

{/* subject section */}
                <div class="inputWithIcon  col-md-12 mt-4 ">
                 <input type="text" class="form-control" placeholder="Subject" />
                   <h3> <i class="fa fa-file-text" aria-hidden="true"></i></h3> 
                 </div>

{/* Message section */}
                 <div class="inputWithIcon   col-md-12 mt-4  ">
                 {/* <input type="text" class="form-control"  rows="5" /> */}
                 <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Message" rows="5"></textarea>
                 </div>

              
             </form>
             <button type="button" class="btn btn-outline-info mt-4 ml-3"><b>Submit</b></button>
            
                </div>
             
             </div>
            </div>
        )
    }
}

export default ContactForm;
