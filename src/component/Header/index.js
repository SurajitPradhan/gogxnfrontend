import React ,{Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../Header/style.css'
import logo4 from '../../images/logo4.png'
import signin from '../../images/signin.png';
import offimage from '../../images/flatoff2.jpg'
import cart from  '../../images/cart.png';
import signup from '../../images/signin2.png'
import { Modal,Button, OverlayTrigger, NavLink } from 'react-bootstrap';
import { Popover } from 'react-bootstrap';
import axios from 'axios'
import {Link,BrowserRouter as Router,Switch,Route, Redirect} from 'react-router-dom'
import AfterNav from '../AfterNav/index.js'
import server from '../server/server';
import ProteinProduct from '../ProtienProduct';
import { ReactSession } from 'react-client-session';
import Navbar from '../Navbar';


class Header extends Component{
    constructor(props){
        super(props);
        this.state={
              cart:false,
              searchdata:'',
              search:false,
              cartitem:0,
              scrollingLock: false,
              productPage:false,
              findProduct:'',
              productName:[],
              username:localStorage.getItem('Name'),
              useremail:localStorage.getItem('email')
        }
        this.SignupClick=this.SignupClick.bind(this)
        this.CartValue=this.CartValue.bind(this)
        this.SingIn=this.SingIn.bind(this)
        this.OffSignin=this.OffSignin.bind(this)
        this.handleScroll=this.handleScroll.bind(this)
        this.ProductPage=this.ProductPage.bind(this)
        console.log(this.props)
        this.SignOut=this.SignOut.bind(this)
    }


    componentDidMount(){
      window.addEventListener('scroll', this.handleScroll);
     
  }
  
  componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
  }
  
  handleScroll() {

    if (window.scrollY > 70) {

;       this.setState({
        scrollingLock:true
         })

    } else if (window.scrollY < 70) {

        this.setState({
          scrollingLock:false
        })
    

    }}
    
    SignupClick(){
      this.setState({
        signup:!this.state.signup
      })
    }

   
    CartValue(){
      this.setState({
         cart:!this.state.cart
      })
    }

    

    SingIn(){
        this.setState({
          signin:!this.state.signin,
        })
    }

    OffSignin(value){
      this.setState({
        signin:value
      })
    }
  

serach(key){
   axios.get(`${server}api/product/?search=`+key).
   then(response=>{
     let NameProduct=[]
    for(var i=0;i<response.data.length;i++){
        if(this.state.findProduct==response.data[i].id){
          NameProduct.push({name:i,value:response.data[i].id})
        }
         
    }
    this.setState({
       productName:NameProduct
    })
 
    console.log(this.state.productName)
   


    if(key){
    this.setState({
      searchdata:response.data,
      search:true
    })
  }else{
    this.setState({
      search:false
    })
  }

     
   })

}


ProductPage=(v)=>{
  this.setState({
       productpage:true,
       findProduct:v,
  })
  
}



SignOut(){

  localStorage.removeItem('Name')
  localStorage.removeItem("Email")
  localStorage.removeItem('cart')
  this.MakeRefresh()
}


MakeRefresh=()=>{
  setTimeout(()=>{
    window.location.reload()
  },500)
  
}




render(){
 
    return(
       
      <React.Fragment>

           {this.state.productpage?<div><Redirect  to={{pathname:`/productpage/${this.state.findProduct}` ,state:{id:this.state.findProduct}}}   />{this.setState({
             search:false,
             productpage:false

           })}</div>:''}

           

   

           
         <div className='navbardesktop' className='float-panel' style={{ width: "100%", position: this.state.scrollingLock ? "fixed" : "relative",top:'0px',zIndex:'100',transition: 'top 2s ease-in' }}>
          {/* <div className="offertext mx-auto">
            <img src={offimage}  className='img-fluid strip' alt='image' />
         </div> */}


<div class="navbars">
   <nav class="nav_bar "style={{backgroundColor:'white'}} >
  <div class="container " >

    <ul className='navbar col-11 mb-0'>
      <li className='nav-item'><Link to='/home'> <img src={logo4} alt="" className='logo'/> </Link> </li>

      <li className='nav-item searchitem'>
      
        <OverlayTrigger
          show={this.state.search}
          placement='bottom-start'
          hideArrow={true}
          overlay={(<div  className="searchbar" style={{zIndex:'200'}}  >
              { this.state.search?
        <div>{this.state.searchdata.map((item)=> <h5 onClick={()=>this.ProductPage(item.name)} className="searchresult" key={item.id}><img src={item.image2} style={{height:'30px',width:'30px'}} alt='loadimage' /> {item.name}</h5> )}</div>:''
              }

          </div>)}
          >
         <input type="search"  name='search'   onChange={(event)=>{this.serach(event.target.value)}} className="nav-link size active form-control me-2" placeholder="Search here..." aria-current="page" />
          </OverlayTrigger>
       
      </li>


  
      
    <li className='nav-item'>
 
      {localStorage.getItem("Email")?<div >
 
      <OverlayTrigger
          trigger='hover'
          placement='bottom'
          overlay={(<Popover style={{padding:'10px'}}>
            Logout
          </Popover>)}
          >
          <i onClick={this.SignOut} style={{color:'white',fontSize:'23px',cursor:'pointer'}} class="fa fa-sign-out" aria-hidden="true" style={{color:'black'}}></i>
          </OverlayTrigger>

      </div>:<div>
        
      <Link to='/signup'> 
      <OverlayTrigger
          trigger='hover'
          placement='bottom'
          overlay={(<Popover style={{padding:'10px'}}>
            Signup
          </Popover>)}
          >
          <h3 onClick={this.SignupClick}><i style={{color:'white',}}  class="fa fa-user-plus" aria-hidden="true" style={{color:'black'}}></i></h3>
          </OverlayTrigger>
    
      </Link>

        </div>}
      </li>
      
    

      <li className='nav-item'>

        {localStorage.getItem('Email')?<div style={{color:'black',cursor:'pointer'}}><p style={{textAlign:'center'}}><i class="fa fa-user" aria-hidden="true"style={{color:'black'}}></i></p> <p style={{textAlign:'center'}}>{localStorage.getItem('Name')}</p></div>:<div>
          
        <Link to='/sigin'>  
      <OverlayTrigger
          trigger='hover'
          placement='bottom'
          overlay={(<Popover style={{padding:'10px'}}>
            Signin
          </Popover>)}
          >
            <h3 onClick={this.SingIn}><i style={{color:'white'}} class="fa fa-sign-in" aria-hidden="true" style={{color:'black'}}></i></h3>
          
          </OverlayTrigger>
       </Link>
          </div>
          }

      </li>
   
        

      <Link to='/cartvalue'> 
      <li className='nav-item' >
             
          <OverlayTrigger
          trigger='hover'
          placement='bottom'
          overlay={(<Popover style={{padding:'10px'}}>
            Your cart is {localStorage.getItem('cart')?localStorage.getItem('cart'):'0'}
          </Popover>)}
          >
         
          <h3 onClick={this.CartValue} ><i style={{color:'white',}} class="fa fa-shopping-cart" aria-hidden="true" style={{color:'black'}}></i><span style={{color:'white',fontSize:'15px',position:'absolute',backgroundColor:'red',width:'20px',textAlign:'center', border:'2px solid black', borderRadius:'5px'}}>{localStorage.getItem('cart')?localStorage.getItem('cart'):0}</span></h3>
          </OverlayTrigger> 
      </li> 
      </Link>

      <Link to='/wishlist'>
      <li className='nav-item' >
             
          <OverlayTrigger
          trigger='hover'
          placement='bottom'
          overlay={(<Popover style={{padding:'10px'}}>
            Wishlist
          </Popover>)}
          >
          
     <i style={{color:'white',fontSize:'22px'}} class='fa fa-heart' style={{color:'black'}}><span style={{color:'white',fontSize:'15px',position:'absolute',backgroundColor:'red',width:'20px',textAlign:'center',borderRadius:'5px'}}>0</span></i>
        
          </OverlayTrigger> 
      </li> 
      </Link>

    </ul>
  
  </div>
</nav>
</div>



</div>

<Navbar/>


</React.Fragment>

    )
}

}





export default Header;
