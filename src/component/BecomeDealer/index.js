import React, { Component } from 'react'
import './style.css';
class BecomeDealer extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             
        }
    }
    
    Buttonclick(){
        alert('Thanks for your enrollment, will be back soon..')
    }
    render() {
        return (
           
     <div className="become_a_Dealer  ">
                <h1 className="become_dealer pt-2 pb-3"><b>Apply for Distributorship</b></h1>
                <div className="enroll_dealership ">
                
                <div className="dealership col-12 ">
                
                <form>
{/* Name section */}
                <div class="inputWithIcon col-md-12 mt-2 ">
                 <input type="text" class="form-control" placeholder="Your name" />
                 <h3><i class="fa fa-user " aria-hidden="true"></i></h3> 
                 </div>

{/* email section */}
            
             <div class="inputWithIcon  col-md-12 mt-4 ">
                 
             <input type="email" class="form-control" placeholder="Email"/>
             <h3><i class="fa fa-envelope" aria-hidden="true"></i></h3>  
             </div>
{/* PhoneNumber section */}
            <div class="inputWithIcon col-md-12 mt-4 ">
              {/* inputIconBg */}
            <input type="number" class="form-control" placeholder="Phone No." />
            <h3>  <i class="fa fa-phone-square" aria-hidden="true"></i></h3> 
                </div>
              

{/* Country section */}
                <div class="inputWithIcon  col-md-12 mt-4 ">
                 <input type="text" class="form-control" placeholder="Country" />
                   <h3> <i class="fa fa-globe" aria-hidden="true"></i></h3> 
                 </div>

{/* city section */}
                 <div class="inputWithIcon  col-md-12 mt-4 ">
                 <input type="text" class="form-control" placeholder="City" />
                   <h3><i class="fa fa-map-marker" aria-hidden="true"></i></h3> 
                 </div>


{/* Company name section */}
                 <div class="inputWithIcon  col-md-12 mt-4 ">
                 <input type="text" class="form-control" placeholder="Company" />
                   <h3><i class="fa fa-briefcase" aria-hidden="true"></i></h3> 
                 </div>

                
{/* Message section */}
                 <div class="inputWithIcon   col-md-12 mt-4  ">
                 {/* <input type="text" class="form-control"  rows="5" /> */}
                 <textarea class="form-control" id="exampleFormControlTextarea1" placeholder="Message" rows="5"></textarea>
                 </div>

              
             </form>
           
         
             <button type="button" class="btn btn-outline-info mt-4 ml-3 mb-5"><b>Submit</b></button>
            
                </div>
         </div>    
             </div>
           
       
        
        )
    }
}

export default BecomeDealer;
