import axios from 'axios'
import React, { Component } from 'react'
import { Redirect } from 'react-router'
import server from '../server/server'
import './style.css'
export class Blogsection extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             data:[],
             individual:false,
             blogname:''
        }
        this.IndividualBlog=this.IndividualBlog.bind(this)
    }
    componentDidMount(){
        axios.get(`${server}api/blogsection/`)
        .then((res)=>{
            let datavalue=[]
           for (var i = 0; i < res.data.length; i++){
                 datavalue.push({name:i,value:res.data[i]})
           }

           this.setState({
                 data:datavalue
           })
         
        })
    }



    IndividualBlog(v){
       
        this.setState({
            individual:true,
            blogname:v
        })
    }
   
    render() {
        return (<>
           {
                            this.state.data.map((item)=>{
                                return(<> 

                                {this.state.individual?<Redirect to={{pathname:`/individualblogpage/${this.state.blogname}`,state:{id:this.state.blogname}}}/>:''}
                            
            <div class="carding" onClick={()=>this.IndividualBlog(item.value.title)}>
            <div class="row no-gutters">
                <div class="col-sm-5">
                    <img class="card-img" style={{cursor:"pointer"}} src={item.value.image} alt="armour whey" />
                </div>
                <div class="col-sm-7">
                    <div class="card-body">
                        <h5 class="card-title " style={{cursor:"pointer"}}><b>{item.value.title}</b></h5>
                        <p class="card-text"  style={{cursor:"pointer", fontSize:"14px"}} >
                      {item.value.text}</p>
                
                    </div>
                    
                </div>
          </div>
        </div>      <hr/>

                            
                            {/* Mobile compertable */}
                         
                              </>  )
                            })
                        }
            </>
        )
    }
}

export default Blogsection
