import React,{Component} from 'react'
import '../signin/style.css';
import logo4 from '../../images/logo4.png';
import axios from 'axios';
import { Redirect, Router, useHistory } from 'react-router';
import {browserHistory} from 'react-router'
import { Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import server from '../server/server';

class Signin extends Component{
  constructor(props){
    super(props);
    this.state={
      email:'',
      password:'',
      error:'',
      success:'',
      loading:false,
      redirect:false,
    }
    
    this.ButtonSignin=this.ButtonSignin.bind(this)
    this.ChangeSignin=this.ChangeSignin.bind(this)
    this.Makingredirect=this.Makingredirect.bind(this)
    
  }


ChangeSignin(event){
    this.setState({
      [event.target.name]:event.target.value
    })
}  

Makingredirect(){
  
  
  setTimeout(()=>{
    this.setState({
      redirect:true
    })
    window.location.reload();
  },2000)

}





 ButtonSignin(){

  this.setState({loading:true})
     
       if(!this.state.email){
          this.setState({
              error:'Email is Mandatory',
              success:'',
              loading:false
          })
       }
       else if(!this.state.password){
        this.setState({
            error:'Password fill not be null',
            success:'',
            loading:false,
            login:false
        })
          
       }
       else{
        axios.get(`${server}api/contact/`).then(res=>{
               var passcheck=false;
               var emailcheck=false;
               var name=''
               var email=''
             if(res.data!=''){
             for(var i=0;i<res.data.length;i++){
               if(this.state.email==res.data[i].email){
                 emailcheck=true;
                  if(this.state.password==res.data[i].password){
                         name=res.data[i].name;
                         email=res.data[i].email
                         passcheck=true;
                  }else{
                         passcheck=false;
                  }
                 break;
               }
               else{
                 emailcheck=false;
               }
             }
             if(emailcheck==false){
                 this.setState({
                     error:'Account is not registered please register',
                     success:'',
                     loading:false
                 })
               passcheck=true;
             }
     
             if(passcheck==false){
                 this.setState({
                     error:'Password not match',
                     success:'',
                     loading:false
                 })
             }
           
             if(emailcheck && passcheck){
               
                  this.setState({
                    error:'',
                    success:'You have Successfully login please wait for loading..',
                    loading:true,
                    email:'',
                    password:'',
                    login:false,  
                  })
                  localStorage.setItem("Name",name)
                  localStorage.setItem("Email",email)
                  this.Makingredirect();
             }  
              
     
           }else{
             this.setState({
               error:'No data found',
               success:'',
               loading:false
           })
           }
        })
       }

 }


 render(){
   return(

    <React.Fragment>

      {this.state.redirect?<Redirect to='/home'/>:''}


     <div className=' signinrender rounded border mb-3 text-center' style={{zIndex:'20'}} >

    <img class="mb-4" src={logo4} alt="" width="180" height="51"/>
    <h1 class="h3 mb-3 fw-normal">Please sign in</h1>

    <div>

  {this.state.error?<div class="alert alert-danger col-5   mx-auto" role="alert">
  {this.state.error}
</div>:''}
  {this.state.loading?<div class="spinner-border text-danger" role="status">
  <span class="sr-only">Loading...</span>
</div>:""}


    </div>
    
    <input autoComplete='off' type="email" id="inputEmail" value={this.state.email} name="email" onChange={this.ChangeSignin} className="row col-6 inputforsignin  mt-1 mx-auto" placeholder="Email address" required autofocus/>
   
    <input autoComplete='off' type="password" id="inputPassword" value={this.state.password} onChange={this.ChangeSignin} name='password'  className="row col-6 inputforsignin  mt-1 mx-auto" placeholder="Password" required autofocus/>
  
    <input className=" btn btn-lg mt-2 btn-success" onClick={this.ButtonSignin} value={this.state.loading?'Loading..':'Login'} type="submit"/>

    <p> <Link to='/signup'><h5 className='signupshift'>Create your account</h5></Link> </p>
    
    <p class="mt-5 mb-3 text-muted">&copy; 2011-2021 Gxn.com</p>
      
     </div>




     </React.Fragment>
   )
 }
}

export default Signin;