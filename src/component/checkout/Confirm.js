import React, { Component } from 'react'
import List from '@material-ui/core/List'
import ListItemText from '@material-ui/core/ListItemText'
import Button from '@material-ui/core/Button'
import logo4 from '../../images/logo4.png'

export default class Confirm extends Component {
 
     
    
    MethodNext=()=>{
        this.props.methodNext();
    }

    MethodPrev=()=>{
        this.props.methodPrev();
    }

    render() {
        const {data}=this.props;
        return (
           <React.Fragment>
                 <div className="container col-9 border rounded text-center mt-2 p-4 bg-light">
                 <img  src={logo4} width="180" height="51"/>
                 <h5 style={{fontWeight:'bold',marginTop:'2px'}}>Making Confirmation Before Payment</h5>
                  <List>
                      <ListItemText
                      primary="Name"
                      secondary={data.Name}
                      />

                     <ListItemText
                      primary="Email"
                      secondary={data.email}
                      />

                    <ListItemText
                      primary="Phone"
                      secondary={data.phone}
                      />

                   <ListItemText
                      primary="Address"
                      secondary={data.address}
                      />


                   <ListItemText
                      primary="Pin"
                      secondary={data.pin}
                      />


                    <ListItemText
                      primary="State"
                      secondary={data.origin}
                      />



                   <Button style={{backgroundColor:'rgb(209, 79, 18)',color:'white'}} onClick={this.MethodPrev}>Previous</Button>    
                 <Button style={{backgroundColor:'rgba(0, 255, 255, 0.637)',margin:'3px'}} onClick={this.MethodNext} >Save & continue</Button>
                 <p style={{marginTop:'3px',color:'green',fontWeight:'bold'}}>Step 3/4</p>

                  </List>

                  </div>
           </React.Fragment>
        )
    }
}
