import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import logo4 from '../../images/logo4.png'

export default class Payment extends Component {

 MakeNext=()=>{
     this.props.methodNext();
 }

 MakePrev=()=>{
     this.props.methodPrev();
 }

    render() {
        return (
            <React.Fragment>
                <div className="text-center container col-9 border rounded bg-light">
                <img  src={logo4} width="180" height="51"/>
                <h4 style={{fontWeight:'bold',marginTop:'2px'}}>this is for the payment section</h4>
                
                <Button style={{backgroundColor:'rgb(209, 79, 18)',color:'white'}} onClick={this.MakePrev}>Go Back</Button>
                <Button style={{backgroundColor:'rgba(0, 255, 255, 0.637)',margin:'3px'}} onClick={this.MakeNext} >Make Your Payment</Button>
                <p style={{marginTop:'3px',color:'green',fontWeight:'bold'}}>Step 4/4</p>
                </div>
                
            </React.Fragment>
        )
    }
}
