import React,{Component} from 'react'
import Cart from '../CartPage';
import Header from '../Header';
import Address from './Address';
import Confirm from './Confirm';
import Payment from './payment';

import './style.css'

class Checkout extends Component{
    constructor(){
        super();
        this.state={
          step:1,
          Name:'',
          email:'',
          phone:'',
          address:'',
          pin:'',
          city:'',
          origin:'',
          buynow:true
        }
    }

    NextStep=()=>{
        const {step}=this.state;

        this.setState({
            step:step+1
        })
    }

    PrevStep=()=>{
        const {step}=this.state

        this.setState({
            step:step-1
        })
    }


    HandleInput=(input)=>e=>{
            
        this.setState({
            [input]:e.target.value
        })
    }
  

    render(){
        const {step}=this.state;
        const {buynow} = this.state;
        const {Name,email,phone,address,city,pin,origin}=this.state;
        const value ={Name,email,phone,address,city,pin,origin}
       
        switch(step){
            case 1:
                return <Address
                 methodNext={this.NextStep}
                 data={value}
                 change={this.HandleInput}
                />
            case 2: 
                return <Cart
                 methodNext={this.NextStep}
                 methodPrev={this.PrevStep}
                 values={value}
                 data={buynow}
                />
            case 3:
                return  <Confirm
                   methodNext={this.NextStep}
                   methodPrev={this.PrevStep}
                   data={value}
                />
            case 4:
                return <Payment
                methodNext={this.NextStep}
                methodPrev={this.PrevStep}
                data={value}
                />
            default:
                return <h1>Your are exceded the limit</h1>
        }
    }
}

export default Checkout;