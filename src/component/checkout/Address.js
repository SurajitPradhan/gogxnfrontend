import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import logo4 from '../../images/logo4.png'

export default class Address extends Component {

    constructor(props){
        super(props);
        this.state={

        }

        this.MakeContinue=this.MakeContinue.bind(this)
    }

   MakeContinue=()=>{
       this.props.methodNext();
   }

    render() {
            const {data,methodNext,change} =this.props;
            

        return (
            
            <React.Fragment>

                <div className="text-center border rounded col-9 container mt-2 p-4 bg-light">
                
                <img  src={logo4} width="180" height="51"/>

                <h4 style={{fontWeight:'bold',marginTop:'2px'}}>Enter Your address Here</h4>
                
                <TextField 
                 className="col-5"
                 placeholder="Enter your Name"
                 label="Name"
                 defaultValue={data.Name}
                 onChange={change('Name')}
                /> <br/>

             <TextField 
                  className="col-5"
                 placeholder="Enter your Email"
                 label="Email"
                 defaultValue={data.email}
                 onChange={change('email')}
                /><br/>


             <TextField 
                 className="col-5"
                 placeholder="Enter your Phone"
                 label="Phone"
                 defaultValue={data.phone}
                 onChange={change('phone')}
                /><br/>


            <TextField 

                className="col-5"
                 placeholder="Enter your address"
                 label="Address"
                 defaultValue={data.address}
                 onChange={change('address')}
                /><br/>

            <TextField 
                className="col-5"
                 placeholder="Enter your Pin"
                 label="Pin"
                 defaultValue={data.pin}
                 onChange={change('pin')}
                /> <br/>

                <TextField 
                className="col-5"
                 placeholder="Enter your City"
                 label="City"
                 defaultValue={data.city}
                 onChange={change('city')}
                /> <br/>

               <TextField 
                className="col-5"
                 placeholder="Enter your State"
                 label="State"
                 defaultValue={data.origin}
                 onChange={change('origin')}
                /> <br/>
            
            <Button onClick={this.MakeContinue}   style={{backgroundColor:'rgba(0, 255, 255, 0.637)',marginTop:'3px'}}>continue</Button>
            <p style={{marginTop:'3px',color:'green',fontWeight:'bold'}}>Step 1/4</p>
            </div>
            
            </React.Fragment>
        )
    }
}
