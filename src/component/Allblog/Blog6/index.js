import React from 'react';
import './style.css';
import blog6 from '../../../Image/blog6.jpg';
const Blog6=()=>{
    return(
        <div class="blog_s px-5">
            <img src={blog6} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog6">
                <h3 className="blog_sec_head">HOW TO RECOVER QUICKLY AFTER A STRENUOUS WORKOUT SESSION?</h3>
                <p className="blog_sec_para">Bodybuilding is a process which includes three major steps Diet, Workout and 
                recovery. Most of the bodybuilders are concerned about the first two but the third one is important 
                amongst all. Whether you’re an occasional runner or a gym fanatic, you should consider recovery an 
                essential part of your health.</p>
                <p className="blog_sec_para"><b>What is muscle recovery?</b></p>
                <p className="blog_sec_para">During the workout session, our muscle tissues break Recovery also
                 allows the body to replenish energy stores and repair damaged tissues. Soreness of muscle after 
                 a workout is a sign of muscle tissues
                 damage which need be recovered in minimal duration.</p>
                 <p className="blog_sec_para"><b>How to speed up muscle recovery?</b></p>
                 <p className="blog_sec_para"><b>Stretching:</b></p>
                <p className="blog_sec_para">An endurance workout session always is followed by a good stretching. This aspect of training is very crucial in the recovery process. It helps to relax your muscles, prevents scar tissues and muscle imbalances. Apart from recovery, it also keeps the muscles flexible, strong, and healthy, 
                and we need that flexibility to maintain a range of motion in the joints.</p>
                <p className="blog_sec_para"><b>Post-Workout Diet:</b></p>
                <p className="blog_sec_para">As per the researches by the fitness group, there is, on an average 30 minutes’ window after the workout, to refuel your body and prevent muscle tissue breakdown. Your body is in demanding state after the grueling workout, screaming for the boost. If you miss on this, not only you push your recovery further away, but to great extent, you neutralize your workout as well. A supplement like ISO legend can be the best choice of someone to
                     fulfill the nutrition demand after the workout.</p>
                     <p className="blog_sec_para"><b>Good Sleep:</b></p>
                     <p className="blog_sec_para">Sleep plays an important part in recovery. Any muscle group that you work heavily deserves a full 48 hours recovery before you challenge it again. Paradoxically, fewer intense sessions and more recovery might help you reach your goals more quickly than filling up your days with as much training as you can manage. Our bodies 
                         repair and get stronger during the ‘deep sleep’ part of the sleep cycle.</p>
                         <p className="blog_sec_para"><b>Steam shower:</b></p>
                         <p className="blog_sec_para">Submerging your body in steam water can reduce soreness and inflammation for up to 24 hours after exercise. Warm droplets of water clear the holes and let skin breath and shine. It relaxes you physically and mentally.

</p>
<p className="blog_sec_para"><b>Hydration:</b></p>
<p className="blog_sec_para">Although, hydration is important for even normal people who are not much involved in physical activity a fitness enthusiast needs to pay more attention to it. Stay well hydrated during gym session: roughly 1 liter of water for every hour of your workout. Drink steadily and regularly – don’t take it all at once.</p>
<p className="blog_sec_para">Dehydration hinders our digestion and absorption which could be the major drawbacks for recovery.

</p>
<p className="blog_sec_para"><b>Supplements:</b></p>
<p className="blog_sec_para">Whey protein, BCAA, and EAA are the common supplements we prefer for the recovery.</p>
<p className="blog_sec_para">So, these are the important aspects that matter in repair and recovery of broken muscle tissues. If you could succeed to maintain the above-mentioned point for rapid recovery, then you can definitely expect to see the great change.

</p>
<p className="blog_sec_para">Thanks!!</p>




                <p className="blog_sec_para">By now you can understand the roles of diet. How selective you need be for the diets according to your goal like for lean or bulky. But the one misconception we need to eradicate permanently what we are following from the ancestor’s point of view that cholesterols are bad for our health. Years and years ago it used to be common knowledge that cholesterol and saturated fats were horrible for you due to high cholesterol and or blood pressure. But now we know that our bodies hormones can’t function or regulate properly with a lack of healthy fats or added cholesterol in our bodies.</p>
                <p className="blog_sec_para">There are numerous and more emerging studies coming out more that we were wrong about fats back in the day. if this was all true than eggs would be one of the worst foods we could possibly have due to the cholesterol content.</p>
                <p className="blog_sec_para">But now we realize that our bodies need certain fats for multiple hormone functions, add in someone who is weight lifting regularly and foods such as steak and eggs should be at first in their diet.</p>
                <p className="blog_sec_para">After knowing all this steal people failed to maintain the perfect diet plan, and to resolve this supplements becomes favorite at first place. Now we have verities of finest nutrition like ISO legend for Lean muscle building, Hardcore mass gainer for swift muscle gain and Hyper Ripped to cut the extra calories in no time. Because we can’t just rely on the intense workout, quality nutrition is one that make difference.</p>
                <p className="blog_sec_para">So, if you want to see the change in you then you need the change the kinds of nutrition you prefer. Explorer the nutrition of India’s top supplement brand Greenex Nutrition and pick the products suitable for you.</p>
                <p className="blog_sec_para">Thanks</p>
            </div>

        </div>
    )
};
export default Blog6;