import React from 'react';
import './style.css';
import blog8 from '../../../Image/blog8.jpg';
const Blog8=()=>{
    return(
        <div class="blog_s px-5">
            <img src={blog8} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog8">
                <h3 className="blog_sec_head"> STEAL THE INTENSITY FROM DEFINED HEALTH SUPPLEMENT: ISO LEGEND</h3>
                <p className="blog_sec_para">In the urban area, the recent dreams of many youngsters are to wake up early. Even when we set the consecutive alarms with an interval of 5 minutes still we fail to stop our hands from pressing the snooze button.
</p>
                <p className="blog_sec_para">A few decades ago it was an effortless activity. Because people back then were quite multifarious and used to be more active then now.
</p>
                <p className="blog_sec_para"> Bundles of thoughts come in our mind when we look for the “reasons” what making us inactive and lethargic day by day. Are the foods we eat or the unhealthy environment we live in? Are those electronic devices which keep us awake all night or our procrastinating behaviors of watching TV series one after another? Our hectic lifestyle or lack of concern about health? etc..</p>
                <p className="blog_sec_para">You might have picked your reason what belongs to your bucket, the reason could be one, few or all. But one reason is common amongst all, even if you agree or not is “Food”. A glimpse of thought of trying something spicy street food on a rainy day left us storing bad cholesterol and stubborn fat.</p>
                <p className="blog_sec_para">But it’s not that, our ancestors were deprived of these kinds of foods but their regular work schedule used to be hectic enough to deplete those elements on the same day. Lifestyles of individuals have changed now as the quality of foods.
</p>
                <p className="blog_sec_para">So, for now, we need to give utmost attention to the kinds of foods we eat rather focusing on how to remove after storing. Because diet plays a major role in making your day active or inactive.
</p>
                <p className="blog_sec_para">Your physique is the best mirror of the foods you prefer. Moreover, routine and discipline workout has its importance.
</p>
                <p className="blog_sec_para">Certainly, it’s easier to manage your workout than diet plan because of the hectic lifestyle and lack of information about the source of nutrition and foods. That is why supplements positioned their space amidst bodybuilders and athletes quite firmly. But still, information about ingredients is needed either its normal diet or dietary supplements. Following this, let me introduce with one of such kinds of health supplement which is appreciated by more than 95% of the user.</p>

                <p className="blog_sec_para"><b>ISO Legend:</b></p>
                <p className="blog_sec_para">It’s an isolate whey protein.</p>
                <p className="blog_sec_para">Isolate whey protein is a type of whey protein which contains the highest amount of protein with almost zero carbs presences. This category of supplement mainly preferred for weight loss and lean muscle building.</p>
                <p className="blog_sec_para">There are tons of isolate whey protein available in marker but how ISO legend managed to gain its fans base is defiantly a matter of concern. So, let’s learn about a few impeccable features about this protein.</p>
                <h5 className="blog_sec_subheads">ISO Legend Ingredients.</h5>
                <ul className="blog_sec_list"><li> It contains di-peptides (amino acid pairs) and tri-peptides (amino acid triples). These are keys that fit specific genetic locks in the body to turn on protein functions </li>
                <li className="blog_sec_ul">Followed by this, this is enriched with Amino acids profile (Isoleucine, Leucine, and valine) that your body needs to repair muscle after a workout             </li>
                <li className="blog_sec_ul">It is free from added sugar and doping ingredients.</li>
                <li className="blog_sec_ul">Fortified with digestive enzymes.</li>
                </ul>

                <p className="blog_sec_para">Uses and Benefits of ISO Legend.</p>
                <ul className="blog_sec_list">
                <li className="blog_sec_ul">Supports in Lean Muscle Building.</li>
                <li className="blog_sec_ul">High in protein, BCAA, and EAA</li>
                <li className="blog_sec_ul">Accelerate the pace of muscle repair and recovery.</li>
                <li className="blog_sec_ul"> Boost strength and stamina.</li>
                <li className="blog_sec_ul">Fast digestion and absorption.</li>
                </ul>
                <p className="blog_sec_para">As you can see how amazingly this product is crafted. It’s just not another pack of supplements promising results unreasonably. 
                    Many majors have been considered at each step during the formulation of this supplement.</p>
                <p className="blog_sec_para">ISO Legend is a product of India’s leading supplement brand Greenex Nutrition. Visit www.gogxn.com for more.
</p>
                          </div>

        </div>
    )
};
export default Blog8;