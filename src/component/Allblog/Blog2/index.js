import React from 'react';
import './style.css';
import blog2 from '../../../Image/blog2.jpg';
const Blog2=()=>{
    return(
        <div class="blog_s px-5">
       
        

  <div className="Blog2 ">
      <h2 className="blog_ss">HOW TO RECOVER QUICKLY AFTER A STRENUOUS WORKOUT SESSION?</h2>
      <p>Bodybuilding is a process which includes three major steps Diet, Workout and recovery. Most of the bodybuilders are concerned about the first two but the third one is important amongst all. Whether you’re an 
          occasional runner or a gym fanatic, you should consider recovery an essential part of your health.</p>
<p><b>What is muscle recovery?</b></p>

<p>During the workout session, our muscle tissues break Recovery also allows the body to replenish energy stores and repair damaged tissues. Soreness of muscle after a workout is a sign of muscle tissues damage which need be recovered in minimal duration.
</p>
<p><b>How to speed up muscle recovery?</b></p>
<p><b>Stretching:</b></p>
<p>An endurance workout session always is followed by a good stretching. This aspect of training is very crucial in the recovery process. It helps to relax your muscles, prevents scar tissues and muscle imbalances. Apart from recovery, it also keeps the muscles flexible, strong, and healthy, and we need that flexibility to maintain a range of motion in the joints.

</p>
<p><b>Post-Workout Diet:</b></p>
<p>As per the researches by the fitness group, there is, on an average 30 minutes’ window after the workout, to refuel your body and prevent muscle tissue breakdown. Your body is in demanding state after the grueling workout, screaming for the boost. If you miss on this, not only you push your recovery further away, but to great extent, you neutralize your workout as well. A supplement like ISO legend can be the best choice of someone to fulfill the nutrition demand after the workout.
</p>
<p><b>Good Sleep:</b></p>
<p>Sleep plays an important part in recovery. Any muscle group that you work heavily deserves a full 48 hours recovery before you challenge it again. Paradoxically, fewer intense sessions and more recovery might help you reach your goals more quickly than filling up your days with as much training as you can manage. Our bodies repair and get stronger during the ‘deep sleep’ part of the sleep cycle.
</p>
<p><b>Steam shower:</b></p>
<p>Submerging your body in steam water can reduce soreness and inflammation for up to 24 hours after exercise. Warm droplets of water clear the holes and let skin breath and shine. It relaxes you physically and mentally.
</p>
<p><b>Hydration:
</b></p>
<h6>WHY DO WE NEED PRE-WORKOUT SUPPLEMENT?</h6>
<ul><li>Boost Muscle Endurance</li>
<li>Accelerate Strength Gain</li>
<li>Stimulate Muscle Building</li>
<li>Increase Extracellular ATP</li>
<li>Maximize training capacity</li>
<li>Enhance N.O production</li>
<li>Blood Circulation</li></ul>
<p>But every athlete has a different goal, for example, one type of athlete might appreciate the increased pain tolerance or force-production it provides during an all-out effort, like a heavy rep or another might appreciate the endurance to knock out a few more reps in high-volume. But to achieve all these goals using pre-workout you must be sceptical about the ingredients pre-workout include.
</p>
<h6>MAIN INGREDIENTS OF PRE WORKOUT SUPPLEMENT</h6>
<ul>
<li>Caffeine for elevated energy</li>
<li>Beta-alanine for better endurance</li>
<li>Creatine for faster recovery</li>
<li>L- citrulline for fast nitric oxide production</li>
<li>BCAAs for more gain</li>
</ul>
<p>Serving size of each important so let me give a perfect example of the best pre workout supplement.</p>
<h6>GXN N.O PUMP PRE WORKOUT SUPPLEMENT</h6>
<p>N.O PUMP pre-workout supplement is a result of extensive research on ingredients and its effects. Each serving of N.O. Pump includes Citrulline DL malate (1500 mg), Beta-Alanine (3000mg), Creatine (1000 mg), and L Arginine (1500 mg). Most importantly it includes the rich amount of Caffeine (250mg), vitamin B3, and B6 which helps increases the focus and concentration during workout.
</p>
<p>This pre-workout supplement can be used by athletes, professional bodybuilders, or someone who is involved in mixed martial arts & extreme sports in optimizing their athletic performance. This product is suitable for both men and women looking for a Pre-workout drink, that delivers Intense Sustained Energy, Pumps & Focus.
</p>
<p>I hope it helps.</p>
 </div>

        </div>
 
 )
};
export default Blog2;