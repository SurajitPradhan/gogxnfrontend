import React from 'react';
import './style.css';
const Blog4=()=>{
    return(
        <div class="blog_s px-5">

                    <div className="blog4">
                <h3 className="blog_sec_head"> HOW TO REDUCE WEIGHT WITH RIGHT STRATEGIES AND MINIMAL EFFORTS?</h3>
                <p className="blog_sec_para">Interest in this blog simply means that you must have typed into Google many a time “How Do I lose Weight ?” Mostly the results pop-up failed to convince you to start your weight loss journey. Because 
                    they don’t actually explain what a person needs to do in order to lose what they don’t want and keep it off.</p>
                    <p className="blog_sec_para">Here we are going to explain you, the far simpler way of approaching “The Weight Loss Goal”.</p>
                    <p className="blog_sec_para">You can lose weight with any number of approaches you may have heard of: IIFYM, paleo, low-carb, 
                       vegetarian, ketogenic, or intermittent fasting. It’s also possible to lose by doing nothing more than eating
                        good food in a moderate amount. Yes, you learned right that sustainable physique transformation happens by</p>
                <ul className="blog_sec_list"><li className="blog_sec_ul">Making healthy alterations to your diet.</li>
                <li className="blog_sec_ul">Controlling your overall calorie intake.</li>
                <li className="blog_sec_ul">Exercising regularly.</li>
                <li className="blog_sec_ul">Disciplined sleep order.</li>
                </ul>
                <p className="blog_sec_para">Or, any other approach regular right approach to keep your body clean. But, in the case of people who are 
                    dealing with obesity since long, must need a few expert-backed recommendations what team GXN brings to you.</p>
                    <h5 className="blog_sec_subheads">HOW TO LOSE WEIGHT</h5>
                    <ul className="blog_sec_list"><li className="blog_sec_ul"></li>
                    <li className="blog_sec_ul">You must focus on adding muscle as well rather than just burning fat.</li>
                    <li className="blog_sec_ul">Stick with a balanced nutrition plan with consistent eating habits and track your 
                        calorie and protein intake.</li>
                    <li className="blog_sec_ul">Aim for 1-2 pounds of loss each week, but not much more.</li>
                    <li className="blog_sec_ul">Exercise at least 60 minutes, 3-4 days per week, with some strength training in the mix.</li>
                    <li className="blog_sec_ul">Use a good quality fat burner.</li>
                    <li className="blog_sec_ul">Perform cardio workouts or high-intensity interval training 2-3 days per week.</li></ul>
                    <p className="blog_sec_para">Now, let’s first understand why we need to focus on gaining muscle along with 
                        losing fat. It’s because muscle mass is a fat-free mass. Fat-free mass includes your muscles, organs, bones, and connective tissue. It also includes water weight. In other words, this is what would be left if you removed every single fat cell from your body. Muscle mass is a major component of your fat-free mass, and it should weigh more than your fat mass. Experts of GXN’s suggest that muscle mass has a huge positive impact on our metabolism or “metabolic rate” which is how many calories your body burns for energy.
                         The more muscle mass you have, the more calories you burn, even when you’re not exercising.</p>
                    <p className="blog_sec_para">So, we must have understood that muscle is also the physical engine that powers you through 
                        activity, both in the gym and in life. Gist of this blog is to understand the importance of
                         muscle building during the fat burning process which is mostly ignored by many.</p>
                <p className="blog_sec_para">At last, let me explain you few workouts you must get into for better and faster results.</p>
                <p className="blog_sec_para">Strength and Resistance training: It’s an essential part of holding weight control. Resistance 
                    training also has profound beneficial effects on your bones and joints and helps to prevent
                     osteoporosis (loss in bone mineral density), sarcopenia (loss of muscle mass), and lower-back 
                     pain.</p>
                     <p className="blog_sec_para">Cardio and HIIT: High Intensity Interval Training or HIIT is the simplest and most
                          effective form of workout.</p>
                          <p className="blog_sec_para">Register yourself for prior notification of new blogs, deals, and offer at www.gogxn.com.</p>
            </div>
      
      
        </div>
    )
};
export default Blog4;