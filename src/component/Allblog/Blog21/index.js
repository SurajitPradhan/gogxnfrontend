import React from 'react';
import './style.css';
const Blog21=()=>{
    return(
        <div class="blog_s px-5">
           <img src={blog5} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog21">
                <h2>UNDERSTAND THE IMPACT OF PLANNED DIET IN BODYBUILDING</h2>
                <p>Can you believe that two people who started the gym together and did the same workout all day but
                     get a drastically different result? Yes, It’s true that your gym partner may get the better physique.
                      Do you know why? Just by following the right diet plan. If the right diet plan can make the miracle, 
                      then why don’t you follow the same?</p>
                      <p>Let’s first understand the impact of diet in bodybuilding and then we will move towards how
                           to manage your daily calorie and nutrition requirement quite easily. Diet plays a crucial role 
                           in muscle formation because muscle formation is done by repair and recovery of damaged muscle
                            fibers through a cellular process. It synthesizes the muscle fiber together to form new muscle
                             protein strands or myofibrils and all these recovered myofibrils increase the thickness of muscle
                              tissues to create muscle growth. In the completion of all these process protein is used as a supreme
                               component not only for repairing of muscle tissues but also as the building block of muscle, bones, 
                               cartilage, blood, and skin. Our body also uses protein to make hormones, enzymes and many other body
                                chemicals. If it’s clear that the right amount of protein intake is holding the major share of 
                                muscle building, then its pretty clear that diet which includes high protein should be part of our
                                 regular diet plan. Similarly, for different purposes like for mass gain, fat burn, stamina building
                                  different kinds of nutrition are required which can be consumed through the individual food resources.
                                   Often top bodybuilding experts found saying that importance of diet is almost 60% in bodybuilding while
                                    rest are the workout and daily life discipline like healthy sound sleep, lots of water intake, avoid
                                     junk food etc. we must pay attention to this statement and should accept this constructively.</p>
                                     <p>After knowing the importance of diet the only question arises in our minds first is what are the
                                          foods which can deliver all this essential nutrition. How can we classify the individual foods 
                                          for each requirement? Like what are the rich source of fiber, protein, omega, and various other
                                           vitamins and minerals. It’s not an impossible task but tedious indeed. In that case, quality 
                                           nutrition turns up as a savior for bodybuilders. Sadly, the market does not work that way and
                                            there are lots of fake supplements manufactured to deceive those who dream of a healthy 
                                            physique. That’s why a brand like Greenex Nutrition entered in this arena to provide the
                                             supplements composed of natural ingredients to avoid the health risk. Greenex Nutrition’s 
                                             whey protein is manufactured using grass feed cow’s milk and added glutamine with blends of 
                                             amino acids profile. All ingredients are mixed which are 100% adaptable with the body and
                                              gives positive results. Because supplements are meant to provide the strength and stamina
                                               for the workout and solely responsible for showing the effective results which you can only
                                                expect from the authentic supplements. In both cases, the quality and the authenticity of 
                                                products Greenex Nutrition worked tremendously and come out with the flying colors. We are
                                                 known for the most authentic and genuine supplement supplier of India.</p>
                                                 <p>Greenex Nutrition provides all range of nutrition including whey protein, mass gainer, fat burner
                                                      and workout essential. Here you can avail the variety of products for all types of needs.</p>
               
                                               <p>Thank you.</p>
            </div>

        </div>
    )
};
export default Blog21;