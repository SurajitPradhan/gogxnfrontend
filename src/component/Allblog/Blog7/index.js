import React from 'react';
import './style.css';
import blog7 from '../../../Image/blog7.jpg';
const Blog7=()=>{
    return(
        <div class="blog_s px-5">
             <img src={blog7} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog7">
                <h3 className="blog_sec_head">HOW TO STAY MOTIVATED FOR BODYBUILDING</h3>
                <p className="blog_sec_para">A Goal is something that can be achieved by sheer dedication and strong determination. And to maintain these two you need to stay motivated with firm discipline.</p>
                <p className="blog_sec_para">Especially, In the fitness goal, motivation is the key to success. Your goal could be anything like, lose weight, put on muscle mass or to build solid muscular physique or win Olympiad- in all, you’re going to come up against some challenging days. On those days, you left with two choices either you fasten your belt and step up the ladder or you quit. The irony is this, the tendency of more people is to settle with the later options.
</p>
                <p className="blog_sec_para">In which category you belong to “Makes the Difference”. So, if you’re not quitter but a 
                    believer then you must follow these 3 rules to stay motivated.</p>
                <ul className="blog_sec_list">
                    <li className="blog_sec_ul">Set your solid goal</li>
                    <p className="blog_sec_para">Do not let your eyes wander from one prize to another with each passing day. Forget the rule of priority when you need to achieve something. There should be nothing before and after. Because we lose motivation when we have something to lean on or the lesser priority seat after failure. Bodybuilding goal is one of the most sensitive ones. Where you need to take care of the many aspects a normal people let go unnoticed. Like Your sleep cycle, Your Diet, types of workout, preference of suitable supplement, etc. When you miss any, you see the change in a negative direction.
                         And this change left you demotivated. So, Choose your goal wisely and work on that precisely.</p>
                    <li className="blog_sec_ul">Embrace the improvement</li>
                    <p className="blog_sec_para">A Big Difference is the cumulative addition of many small ones. Bodybuilding is a kind of process
                        where results take time to be seen. You will have to be optimistic about your goal and you need to believe in your hard work. When you appreciate our efforts, it’s very obvious we get inner satisfaction and feel the impulse of taking the step forward for further growth. Similarly, we should embrace the improvement in muscle size and stamina building for the unstoppable workout session.
                             </p>
                    <li className="blog_sec_ul">Be selective about your diet chart</li>  </ul>
                    <p className="blog_sec_para">How lethargic and active we reach is majorly depend on the kinds of food we prefer. A fitness enthusiast neither is gourmand nor capricious about food selection. You have to very particular about what you eat, when you eat. Like a bodybuilder need the diet full of protein, amino and calorie and a minimal amount of good fat. But most of the raw foods are that bodybuilding oriented and comes with some extra lactose and fructose which are the byproduct for the fitness lover. So, if you find difficult to access that kind of food which are rich in protein and have no fat, sugar, and cholesterol then you can go for some quality bodybuilding supplement like ISO legend, Carbo Ex, Gold Whey, etc.</p>
                   <p className="blog_sec_para">These are some of the most preferred supplement by bodybuilders and athletes for the hardcore bodybuilding. Mostly those who are preparing for some kinds of championship and bodybuilding competition has been seen using ISO Legend thoroughly.</p>
                   <p className="blog_sec_para">ISO legend helped me throughout in the journey of winning Mr world 2018 said Benzon thockchom.</p>
                   <p className="blog_sec_para">So, these are the three important factor you need to keep in mind. The goal, Motivation, and Diet.</p>
                   <p className="blog_sec_para">Fixed Goal is needed because it keeps you consistent. Motivation is must to keep up the pace so as diet.</p>
                   <p className="blog_sec_para">I hope you hope to find this article helpful. If yes then please share with others. </p>
           </div>

        </div>
    )
};
export default Blog7;