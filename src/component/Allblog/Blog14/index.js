import React from 'react';
import './style.css';
const Blog14=()=>{
    return(
        <div class="blog_s px-5">
           <img src={blog5} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog14">
                <h3 className="blog_sec_head">OVERTRAINING MAY CAUSE ADVERSE EFFECT</h3>
                <p className="blog_sec_para"><b><i>Access of Anything is Bad!!</i></b></p>
                <p className="blog_sec_para">We have been Listening this quote since so long and its applicable mostly everywhere either action is 
                    right or wrong. Bodybuilding is not something excluded by this protocol. Did you ever heard people injured 
                    badly or suffering some lifelong injuries? Just Because they were not aware with the proper functioning of 
                    exercise and how it impacts on your physique. Often you can see this in beginners or those who are naïve in
                     proper research. What’s seldom is veteran athletes or bodybuilders also doing many few things wrong
                      collectively which need to be told anytime soon. One of those mistakes is “Overtraining”</p>
                      <p className="blog_sec_para">Yes, this is ironically true that many a problem arising in extra-enthusiastic weight 
                          lifters because of overtraining.</p>
                          <p className="blog_sec_para"><b><i>So, what is Overtraining actually?</i></b></p>
                          <p className="blog_sec_para">A continuation of training even after the stipulated time of workout or your body unable to provide the required energy to elongate your workout further. In spite of getting many warning you continue
                               your training without a gap is major mistake you are doing.</p>
                               <p className="blog_sec_para">The situation of overtraining includes a number of symptoms, which can destroy motivation and prevent the bodybuilder of any possibility of continued growth. When striving for beamy increases in muscle, bodybuilders often pushes one’s stamina to the last of limit, without realizing the fact that rest
                                    is as just a part in muscle building is a blunder.</p>
                                    <p className="blog_sec_para">Lack of proper sleep and rest is the very first phase of overtraining. Adequate sleep is needed for all even if they are not involving in intense training as it is the essential need of sedentary body as well. We know that body repair and recovery is the most important phase of muscle development, in this time our body needs an optimum amount of protein and required elements for tissue repairing. When someone ends up breaking massive amount of tissues which was in pre-stage of development and not needed to be touched is obviously a big mistake they are committing for themselves. Many experts had denied that workout for 3 hours is better than 1 hour. Our motive of workout should be to put a strain in muscle rather tear out the grounded muscle. When you should realize
                                         you are trapped under overtraining is mentioned below.</p>
                                         <p className="blog_sec_para"><b>Seven major symptoms when you’re doing overtraining:</b></p>
                                         <ul className="blog_sec_list"> <li className="blog_sec_ul">Unable to lift weight.</li>
                                          <li className="blog_sec_ul">Sleep disturbances.</li>
                                          <li className="blog_sec_ul">No Visible growth.</li>
                                          <li className="blog_sec_ul">Muscle achiness</li>
                                          <li className="blog_sec_ul">Chaotic mind</li>
                                          <li className="blog_sec_ul">Difficulties in weight training.</li>
                                          <li className="blog_sec_ul">Sudden decrement in muscle size.</li>
                                         </ul>
                                         <p className="blog_sec_para">Where, symptoms of overtraining are physical and psychological both it includes, 
                                             elevated waking pulse rate, elevated morning blood pressure, increased joint and
                                              muscle aches, headaches and tremors, tiredness, listlessness, insomnia, loss or
                                               decrease in appetite, injury, illness, chronic fatigue, insatiable thirst or
                                                dehydration, susceptibility to colds and flu, frequent minor infections and
                                                 altered function of the endocrine, immune, and central nervous systems, and 
                                                 increased apathy and irritability, mood and sleep disturbances, depression,
                                                  anxiety, reduced ability to concentrate and lack of appetite.</p>
                                                  <p className="blog_sec_para">So, avoid overtraining and before you hit the gym do some research about the same. You can read more about bodybuilding by visiting Greenex Nutrition blogs.

</p>
<p className="blog_sec_para">Thanks.

</p>
            </div>

        </div>
    )
};
export default Blog14;