import React from 'react';
import './style.css';
import blog9 from '../../../Image/blog9.jpg';
const Blog9=()=>{
    return(
        <div class="blog_s px-5">
           <img src={blog9} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog9">
                <h3 className="blog_sec_head">FEW FACTS YOU ARE NOT AWARE ABOUT BODYBUILDING</h3>
                <p className="blog_sec_para">One of the cliché phrase “workout-eat- sleep-repeat” is not just becoming an adage but also many
                     thinks this the bodybuilding all about. There are several aspects of bodybuilding, many not aware of which are important and must be taken care of.
Today we will discuss the 5 lesser-known facts about bodybuilding which will help to understand the larger picture of muscle
 building.</p>
 <ul className="blog_sec_list"><li className="blog_sec_ul">Importance of Genetics</li>
 <p className="blog_sec_para">Although, training is important factor in muscle building but, genetic predisposition also plays equal 
importance in your muscle growth. A unique gene has individual ability and capabilities to develop muscle mass.
 However, starting from a low base you can always improve your body’s ability. Factors which matter are muscle
  fiber ratio, anabolic hormones, and body type. It’s not a matter to worry about or a moment of exhilaration
   rather a matter of concern that which area you need to work on and not. Because most of these disadvantages
    can be improved with time and by supplements. Like you can maintain your omega-3 level using the quality
     omega-3 fish oil.</p>
     <li className="blog_sec_ul">Mechanism of Weight and Reps</li>
     <p className="blog_sec_para"> Reps is repetitions you do with a certain amount of weight for each weight training exercise set. We often get confused between, whether I should go for the higher weight or more repetitions. Though it depends on your current strength and stamina but you can always follow the strategic training for better results. A pre-decided method of workout session not only helps to prevent the possible injury but also improve the effectiveness of each. So, perform 10 to 15 lifts less than a minute and 10 seconds break between sets. Lactic acid causes that burning sensation in muscles when you exercise intensely and this appears to stimulate muscle growth,
          perhaps from an increase in growth hormone production.</p>
          <li className="blog_sec_ul"> Importance of Weight Training</li>
          <p className="blog_sec_para">Spot workout and compound workout are two different approaches of bodybuilding. Where one is
 mostly preferred by bodybuilders and others by athletes. Spot workout helps in shape building while 
 a compound workout improves strength and stamina. What unites these two is weight training. These are
  the squat, the deadlift and the bench press. They build strength, boost stamina, and improve gain 
  and should always be included in one form or another. So, either you are athlete or bodybuilder
   weight training exercise must be on the top of the list.</p>
   <li className="blog_sec_ul">Diet Cycle</li>
   <p className="blog_sec_para">You might have heard that eating less and the number of times is better than another way. But, what is not quite comprehend by many novices is a cycle of food in a certain duration of time makes your body more aware and habitual for the foods. Which results into quicker digestion and better absorption. When to eat is slightly more important than what to eat. Especially when you’re in weight gain training. If you want to maintain or increase muscle in a weight loss phase, try eating well on the days you exercise in the hour before and after exercise, and cutting back strongly on intake for the days you do not exercise. From Moring, noon to night, the proportion of diet should be in a ratio of 4:2:1.
        Follow the same cycle and similar interval between two meals.</p>
        <li className="blog_sec_ul">The ratio of carbs and protein in your diet</li>
        <p className="blog_sec_para">Myths have been already busted that carb is not only required by those who need to gain. 
            It’s essential for every individual whoever involves in bodybuilding. A calorie is the basic 
            unit of energy and it’s come after the procession of carbs into the body. If you exercise hard 
            and long with cardio, circuits or bodybuilding programs, you need sufficient carbohydrate to fuel 
            your effort and to maintain body stores of glucose. Low-carb diets are not suitable for this type of
             training. Depending on the intensity and volume of your training, you may need 2 to 3.5 grams of
              carbohydrate per pound of body weight per day.</p>
        
 </ul>
 <p className="blog_sec_para">Protein needs no introduction when it comes to bodybuilding. Protein is the building blocks of muscle and for muscle growth and repair you need a good source of protein. About 20 to 30 grams of protein consumed about 45 to 60 minutes before your training may help you to induce a muscle-building effect. Consume the same amount of protein (30 grams) within 20 to 45 minutes after the training combined with some carbohydrate and creatine.
But the question is, what amount of carbs and protein should be blended to make the perfect shake. So, it’s 3:1 if your goal is to get lean and ripped and 2:1 if you need to gain for muscle building.
</p>
<p className="blog_sec_para">If you keep your workout activity and diet plan precisely like I have tried to explain will be helpful in many aspects.
</p>
            </div>

        </div>
    )
};
export default Blog9;