import React from 'react';
import './style.css';
const Blog15=()=>{
    return(
        <div class="blog_s px-5">
             <img src={blog5} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog15">
                <h3 className="blog_sec_head">WHAT IS THE DIFFERENCE BETWEEN CALLISTHENIC AND GYM? WHICH IS BETTER?</h3>
                <p className="blog_sec_para">Callisthenic and gym, these two are not just different in the terms of workout even posture you built are also quite different from each others. Both the activity includes rigorous sets of a workout but they are clustered uniquely and executed differently for exceptional results. 
                    You might have seen people referring to every kind of workout as bodybuilding or gym workout.</p>
                    <p className="blog_sec_para"><b><i>In laymen language, we can differentiate these by saying.</i></b></p>
                    <p className="blog_sec_para"><b>Calisthenics:</b>A system of resistance training in which the resistance used is one’s own bodyweight.</p>
                    <p className="blog_sec_para"><b>Bodybuilding:</b>A sport involving vigorous exercise to strengthen and enlarge one’s musculature.</p>
                    <p className="blog_sec_para">If you compare in sense of domination like calisthenics is more powerful than bodybuilding is like asking if a sports car
                         is better than a SUV. These two has different sets of advantages and importance.</p>
                         <p className="blog_sec_para">Calisthenics is mostly compound exercises that train multiple muscle groups. Calisthenics can give you a reasonably aesthetic physique, good amount of strength that can also be helpful in our day to day activities, core strength, and endurance. Also for people who train alone and for those who cannot afford good workout facilities around calisthenics can be a good option. Whereas bodybuilding is a sport where athletes will do progressive resistance training to increase their mass and musculature this sport demands visual aesthetics.
                              This is only meant to increase muscle and strength for perfect body posture.</p>
                              <p className="blog_sec_para">So, if you ask what you should prefer then it just depends what ’s bestaser for you, what is your goal and expectancy.

                                  </p>
                                    <h5 className="blog_sec_subheads">BENEFITS OF CALISTHENICS:</h5>
                                            <ul className="blog_sec_list">
                                                 <li className="blog_sec_ul">Helpful for regular work and movements.</li>
                                                  <li className="blog_sec_ul">Great for functional strength.</li>
                                                  <li className="blog_sec_ul">Calisthenics is free of cost, no need to buy equipment.</li>
                                                  <li className="blog_sec_ul">Build stamina and core strength.</li>
                                                  <li className="blog_sec_ul">Help to build an absolutely lean and fat-free physique.</li>
                                                  <li className="blog_sec_ul">Prevent obesity to boost heart health.</li>
                                                  <li className="blog_sec_ul">Compound exercise makes your muscle more grounded.</li>
                                                       </ul>
                                             <h6>BENEFITS OF BODYBUILDING:</h6>
                                               <ul className="blog_sec_list">
                                                   <li className="blog_sec_ul">Best to increase muscle size.</li>
                                                   <li className="blog_sec_ul">Helpful to shape your overall physique and improve appearance.</li>
                                                   <li className="blog_sec_ul">Spot based workout. You can improve the specific area for better shape.</li>
                                                   <li className="blog_sec_ul">Boost the immune system and balance overall health functioning.</li>
                                                   <li className="blog_sec_ul">Helps in rapid muscle gain.</li>
                                                   <li className="blog_sec_ul">Improve bone health.</li>
                                                   <li className="blog_sec_ul">Lower cholesterol level stimulates hormones.</li>
                                      </ul>
                                      <p className="blog_sec_para">When it comes to what is more effective then you can see both have their own pros. for example, If you want to push a car for 1 km a calisthenics guy will do better due to their endurance but if you want to push a truck for 100 meter a bodybuilder will do better due to his strength. Both sports are equally respectable and have their own pros and cons and its completely dependent on your requirement that what you are supposed to choose.

</p>
<p className="blog_sec_para">One thing is which is most common in both is diet. You need an adequate amount of protein, carbs, amino, vitamins, and minerals to better performance in each. Infect, you can get all these supplements quite easily from Greenex Nutrition.

</p>
            </div>

        </div>
    )
};
export default Blog15;