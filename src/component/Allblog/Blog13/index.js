import React from 'react';
import './style.css';
const Blog13=()=>{
    return(
        <div class="blog_s px-5">
           <img src={blog5} class="img-fluid" alt="Responsive image"/>
        
            <div className="blog13">
              <h3 className="blog_sec_head">BCAA : WHY THIS IS SO IMPORTANT FOR EVERY BODYBUILDER?</h3 >
              <p className="blog_sec_para">Branched Chain Amino Acids (BCAAs) are very popular within the fitness industry and amongst fitness enthusiasts. 
                  They are often taken as a standalone powder, tablets, and capsules.</p>
                  <p className="blog_sec_para">BCAAs are considered the perfect supplement for people looking to put on mass, but more so 
                      people who are looking to cut down and shred some body fat with maintaining as much muscle as 
                      possible. BCAA is made up of three amino acids: Leucine, Isoleucine, and Valine where leucine is 
                      sort of the spark to ignite a fire. This amino acid signals your muscles to grow. The body needs 
                      a threshold amount of leucine, below which your muscles will not grow. 
                      This means that you need enough leucine for muscle recovery.</p>
                      <h5 className="blog_sec_subheads">HOW DO BCAAS WORK?</h5>
                      <p className="blog_sec_para">When you are cutting calories, your body will be in a catabolic state. This means that your body will be breaking down tissue 
                          (fat, some muscle etc.) rather than making it (which is known as being anabolic.)</p>
                          <p className="blog_sec_para">Muscle loss will occur when cutting since your body opts to use the amino acids which would have otherwise been utilized for protein synthesis (to build muscle) as an energy source. To build muscle, your body’s rate of protein synthesis must be greater than the rate of protein (muscle tissues) breakdown. When it is the other way round, muscle loss occurs, and when they are equal your muscle mass remains the same. So, basic elements
                               of BCAA helps your body to provide a sufficient amount of amino for rapid recovery.</p>
                               <h5 className="blog_sec_subheads">FIVE MAJOR BENEFITS OF BCAAS:</h5>
                               <p className="blog_sec_para"><b>Increase Muscle Growth</b></p>
                               <p className="blog_sec_para">In one study, people who consumed a drink with 5.6 grams of BCAAs after their resistance workout had a 22% greater increase in muscle protein synthesis compared to those who consumed a placebo drink. The BCAA leucine activates a certain pathway in the body that 
                                   stimulates muscle protein synthesis, which is the process of making muscle.</p>
                                   <p className="blog_sec_para">Therefore, one of the most popular uses of BCAAs is to increase muscle growth.</p>
                                   <p className="blog_sec_para"><b>Decrease Muscle Soreness</b></p>
                                   <ul className="blog_sec_list"><li className="blog_sec_ul">Research suggests BCAAs can help decrease muscle soreness after a workout.
                                       </li>
                                       <li className="blog_sec_ul">It’s not uncommon to feel sore a day or two after a workout, especially if your exercise routine is intense or new.</li>
                                       <li className="blog_sec_ul">This soreness is called delayed onset muscle soreness (DOMS), which 
                                           develops 12 to 24 hours after exercise and can last up to 72 hours.
                                            BCAAs also have been shown to decrease muscle damage, which may help
                                             reduce the length and severity of DOMS.</li>
                                             <li className="blog_sec_ul">In one study, people who supplemented with BCAAs before a squat 
                                                 exercise experienced reduced DOMS and muscle fatigue compared to
                                                  the placebo group</li>
                                                  <li className="blog_sec_ul">Therefore, supplementing with BCAAs, especially before 
                                                      exercise, may speed up recovery time.</li>
                                                      
                                        </ul>
                                        <p className="blog_sec_para"><b> Reduce Exercise Fatigue</b></p>
                                        <p className="blog_sec_para">Everyone experiences fatigue and exhaustion from exercise at some point. How quickly you tire depends on several factors, including exercise intensity and duration, environmental conditions and your nutrition and fitness level. Just as BCAAs may help decrease muscle soreness from exercise, they may also help reduce exercise-induced fatigue.

</p>                    
                      <p className="blog_sec_para">Therefore, BCAAs improve mental focus during exercise and reduce exercise fatigue.</p>
                      <p className="blog_sec_para"><b>Prevent Muscle Wasting</b></p>
                      <ul className="blog_sec_list"><li className="blog_sec_ul">BCAAs can help prevent muscle wasting or breakdown</li>
                      <li className="blog_sec_ul">Muscle proteins are constantly broken down and rebuilt (synthesized). The balance between muscle protein breakdown and synthesis determines the amount of protein in muscle.
</li>
                      <li className="blog_sec_ul">Muscle wasting or breakdown occurs when protein breakdown exceeds muscle protein synthesis. In humans, BCAAs account for 35% of the essential amino acids found in muscle proteins. They account for 40% of the total amino acids required by your body
</li>
                      <li className="blog_sec_ul">Therefore, it’s important that the BCAAs and other essential amino acids are replaced during times of muscle wasting to halt it or to slow its progression
</li>
                    
                      </ul>



                      <p className="blog_sec_para"><b>Benefit People with Liver Disease</b></p>
                      <ul className="blog_sec_list"><li className="blog_sec_ul">BCAAs may improve health in people with cirrhosis, a chronic disease in which 
                          the liver does not function properly.</li>
                      <li className="blog_sec_ul">It’s estimated that 50% of people with cirrhosis will develop hepatic encephalopathy, which is the loss of brain function that occurs when the liver is unable to remove toxins from the blood
</li>
                      <li className="blog_sec_ul">While certain sugars and antibiotics are the mainstays of treatment for hepatic encephalopathy, BCAAs may also benefit people suffering from this disease.
</li>
                      <li className="blog_sec_ul">Greenex Nutrition manufactured the impeccable BCAA supplement, Prime BCAA 2:1:1. As we know the pros of this supplement and how it affects the human body we have taken care of all the aspects wisely and came up with the purest form possible. So, utilize this unbeatable source of amino and benefits your body with quality nutrition.
</li>
                    
                      </ul>
            </div>

        </div>
    )
};
export default Blog13;