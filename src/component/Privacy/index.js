import React, { Component } from 'react'
// import './style.css'
export class Privacy extends Component {
    render() {
        return (
            <div className="policies px-5">
                <h1 className="policy_tag mt-4">PRIVACY POLICY</h1>
     <p className="instst">This privacy policy has been compiled to better serve those who are concerned with how 
                    their ‘Personally Identifiable Information’ (PII) is being used online. PII, as described 
                    in US privacy law and information security, is information that can be used on its own or 
                    with other information to identify, contact, or locate a single person, or to identify an 
                    individual in context. Please read our privacy policy carefully to get a clear understanding
                     of how we collect, use, protect or otherwise handle your Personally Identifiable Information 
                     in accordance with our website.</p>
                     <p className="instruction mt-3"><b>What personal information do we collect from the people that visit our blog, website or app?</b></p>
                     <p className="instst">When ordering or registering on our site, as appropriate, you may be asked to enter your
                          name, email address, mailing address, phone number, credit card information or other 
                          details to help you with your experience.</p>
                          <p className="instruction mt-3"><b>When do we collect information?</b></p>
                          <p className="instst ">We collect information from you when you register on our site, place an order,
                               subscribe to a newsletter, respond to a survey, fill out a form or enter information
                                on our site.</p>
                                <p className="instruction mt-3"><b>How do we use your information?</b></p>
                                <p className="instst">We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, 
                            surf the website, or use certain other site features in the following ways:</p>  
                            <ul className="instst pl-4">
              <li className="instst">To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
                                <li className="instst">To improve our website in order to better serve you.</li>
                                <li className="instst">To allow us to better service you in responding to your customer service requests.</li>
                                <li className="instst">To administer a contest, promotion, survey or other site feature.</li>
                                <li className="instst">To quickly process your transactions.</li>
                                <li className="instst">To ask for ratings and reviews of services or products</li>
                                <li className="instst">To follow up with them after correspondence (live chat, email or phone inquiries)</li>
                            </ul>
                            <p className="instruction mt-3"><b>How do we protect your information?</b></p>
                            <ol className="instruction pl-4">
                                <li className="instst">We do not use vulnerability scanning and/or scanning to PCI standards.</li>
                                <li className="instst">We only provide articles and information. We never ask for credit card numbers.</li>
                                <li className="instst">We use regular Malware Scanning.</li>
                                <li className="instst">Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition,
                                     all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</li>
                                <li className="instst">We implement a variety of security measures when a user places an order to maintain the safety of your personal information.</li>
                                <li className="instst">All transactions are processed through a gateway provider and are not stored or processed on our servers.</li>
                            </ol>
                            <p className="instruction mt-3"><b>Do we use ‘cookies’?</b></p>
                            <p className="instst">We do not use cookies for tracking purposes</p>
                            <p className="instst">You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser’s Help Menu to learn the correct way to modify your cookies.</p>
                            <p className="instst">If you turn cookies off, some features will be disabled. that make your site experience more efficient and may not function properly.</p>
                            <p className="instst">However, you will still be able to place orders .</p>

                            <p className="instruction mt-3"><b>Third-party disclosure</b></p>
                             <p className="instst">We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless
                                  we provide users with advance notice. This does not include website hosting partners and other parties who 
                                  assist us in operating our website, conducting our business, or serving our users, so long as those parties
                                   agree to keep this information confidential. We may also release information when it’s release is appropriate
                                    to comply with the law,
                                  enforce our site policies, or protect ours or others’ rights, property or safety.</p>
                                  <p className="instst">However, non-personally identifiable visitor information may be provided to other parties for marketing,
                                       advertising, or other uses.s</p>

                                       <p className="instruction mt-3"><b>Third-party links</b></p>
                                       <p className="instst">Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless,
                                            we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
                                         
                                           <p className="instruction mt-3"><b>Google</b></p>
                                           <p className="instst">Google’s advertising requirements can be summed up by Google’s Advertising Principles. They are put in place to provide a positive experience 
                                               for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en</p>
                                               <p  className="instst">We have not enabled Google AdSense on our site but we may do so in the future.</p>
                                               <p  className="instruction">COPPA (Children Online Privacy Protection Act)</p>
                                               <p  className="instst">When it comes to the collection of personal information from children under the age of 13 years old, the Children’s Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States’ consumer protection agency, enforces the COPPA Rule, which spells out what 
                                                   operators of websites and online services must do to protect children’s privacy and safety online.</p>
                                         <p  className="instst">We do not specifically market to children under the age of 13 years old.</p>

                                         <p className="instruction mt-3"><b>Fair Information Practices</b></p>
                                         <p className="instst">The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented
                                                        is critical to comply with the various privacy laws that protect personal information.</p>
                                             <p className="instst">In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:</p>
                                             <p className="instst">We will notify you via email within 7 business days.</p>
                                                        

                                             <p className="instst">We also agree to the Individual Redress Principle which requires that individuals have the right to legally pursue enforceable rights against data collectors and processors who fail to adhere to the law. This principle requires not only that individuals have enforceable rights against data users, but also that individuals have
                                                             recourse to courts or government agencies to investigate and/or prosecute non-compliance by data processors.</p>

                                                 <p className="instruction mt-3"><b>CAN Act</b></p>
                                                 <p className="instst">The CAN- Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right 
                                                                 to have emails stopped from being sent to them, and spells out tough penalties for violations.</p>
                                                     <p className="instst">We collect your email address in order to:</p>
                                                                 <li className="instst pl-4">Send information, respond to inquiries, and/or other requests or questions</li>
                                                     <p className="instst">To be in accordance with CANSPAM, we agree to the following:</p>
                                                 <ul className="instst pl-4">
                                                  <li className="instst ">Not use false or misleading subjects or email addresses.</li>
                                                  <li className="instst">Identify the message as an advertisement in some reasonable way.</li>
                                                  <li className="instst">Include the physical address of our business or site headquarters.</li>
                                                  <li className="instst">Monitor third-party email marketing services for compliance, if one is used.</li>
                                                  <li className="instst">Honor opt-out/unsubscribe requests quickly.</li>
                                                  <li className="instst">Allow users to unsubscribe by using the link at the bottom of each email.</li>
                                                </ul>
                                                     <p className='instst'>If at any time you would like to unsubscribe from receiving future emails, you can email us at support@gogxn.com and we will promptly remove you from all correspondence.</p>
                                                     <p className="instruction mt-3">Contacting Us</p>
                                                     <p className="instst">If there are any questions regarding this privacy policy, you may contact us using the information below.</p>
                                                     <p className="instst mb-5"><a href="#">www.gogxn.com</a> | support@gogxn.com

</p>

  
            </div>
        )
    }
}

export default Privacy
